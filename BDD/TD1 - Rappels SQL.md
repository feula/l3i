# TD1 - Rappels SQL

### Question 1

```sql

CREATE TABLE Employe (
	  matrempl char(2) PRIMARY KEY,
  
	       nom varchar2(25),
	    prenom varchar2(25),
	   adresse varchar2(50),
	    qualif varchar2(25),
	servaffect char(2)
);

CREATE TABLE Service (
	 nuserv char(2) PRIMARY KEY,

  	libserv varchar2(25),
  	matrdir char(2)
);

CREATE TABLE Projet (
  	   nuproj char(3) PRIMARY KEY,

  	  libproj varchar2(25),
  	 matrchef char(2),
  	datedebut date
);

CREATE TABLE CompoProj(
	     nuproj char(3),
	      nulot char(3),
	 tpstoprevh number(4),
	tpsencoursh number(4),
	tpsencoursm number(2),

	FOREIGN KEY nuproj REFERENCES Projet(nuproj)
);

CREATE TABLE Travail(
	  nuproj char(3),
	   nulot char(3),
	datejour date,
	matrempl char(2),
	tpsjourm number(3),

	FOREIGN KEY   nuproj REFERENCES Projet(nuproj),
	FOREIGN KEY    nulot REFERENCES CompoProj(nulot),
	FOREIGN KEY matrempl REFERENCES Employe(matrempl)
);
```