#include <stdlib.h>
#include <stdio.h>

char* base_converter(char in[], int base_entree, int base_sortie) {
    if (base_entree < 2 || base_entree > 36) {
        printf("La base de départ doit être comprise entre 2 et 36");
        exit(1);
    }
    
    if (base_sortie < 2 || base_sortie > 36) {
        printf("La base de sortie doit être comprise entre 2 et 36");
        exit(1);
    }

    char *tbl = "0123456789abcdefghijklmnopqrstuvwxyz";
    char *endptr;

    long ret;
    int converted_to_base;
    char buf[255] = {'\0'};
    char *out;

    int len = 0;
    int done = 0;
    printf("Input: %s (from %u to %u)\n", in, base_entree, base_sortie);

    // Interprétation du nombre, conversion en base 10
    ret = strtol(in, &endptr, base_entree);
    
    // Conversion vers base appropriée
    while(done != 1) {
        buf[len++] = tbl[ret % base_sortie];
        ret /= base_sortie;

        if (ret == 0) { done = 1;}
    }

    out = malloc(sizeof(char) * (len+1));
    for (int i = 0; i < len; i++) {
        out[i] = buf[i];
    }

    return out;
}

int main(void) {
    printf("Conversion de 3eh12 (base 18) vers base 30: %s \n", base_converter("3eh12", 18, 30)); 
}