#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include "donnees_somme.hpp"

/**
 * Effectue une somme récursive de <elems>, à partir de <idx>
 * @param elems Liste des éléments à sommer
 * @param idx   Index à partir du quel sommer
 * @return double Somme des éléments
 */
double somme_recursive(std::vector<double> elems, uint idx) {	
	if (elems.size() == idx)
		return 0;
		
	return elems.at(idx) + somme_recursive(elems, idx+1);
}

/**
 * Effectue une somme de <elems> trié par ordre croissant
 * @param elems Liste des éléments à sommer
 * @return double Somme des éléments
 */
double somme_croissante(std::vector<double> elems) {	
	double sum;
	
	std::sort(elems.begin(), elems.end(), std::greater<double>());
	for (double it : elems) {
		sum += it;
	}
	
	return sum;
}

/**
 * Effectue une somme de <elems> trié par ordre décroissant
 * @param elems Liste des éléments à sommer
 * @return double Somme des éléments
 */
double somme_decroissante(std::vector<double> elems) {
	double sum;
	
	std::sort(elems.begin(), elems.end(), std::less<double>());
	for (double it : elems) {
		sum += it;
	}
	
	return sum;
}

/**
 * Effectue une somme de <elems> trié par ordre absolu croissant
 * @param elems Liste des éléments à sommer
 * @return double Somme des éléments
 */
double somme_abs_croissante(std::vector<double> elems) {
	double sum;
	struct {
		bool operator()(double a, double b) {
			return std::abs(a) > std::abs(b);
		}
	} tri_abs_croissant;
	
	std::sort(elems.begin(), elems.end(), tri_abs_croissant);
	for (double it : elems) {
		sum += it;
	}
	
	return sum;
}

/**
 * Effectue une somme de <elems> trié par ordre absolu décroissant
 * @param elems Liste des éléments à sommer
 * @return double Somme des éléments
 */
double somme_abs_decroissante(std::vector<double> elems) {
	double sum;
	struct {
		bool operator()(double a, double b) {
			return std::abs(a) < std::abs(b);
		}
	} tri_abs_decroissant;
	
	std::sort(elems.begin(), elems.end(), tri_abs_decroissant);
	for (double it : elems) {
		sum += it;
	}
	
	return sum;
}

/**
 * Effectue une somme itérative de <elems>
 * @param elems Liste des éléments à sommer
 * @return double Somme des éléments
 */
double somme_iterative(std::vector<double> elems) {
	double sum;
	
	for (double it: elems) {
		sum += it;
	}
	
	return sum;
}

int main(int argc, char** argv) {
	std::cout.precision(17);

	std::cout << std::fixed;
	std::cout << "Somme recursive :";
	std::cout << somme_recursive(std::vector<double>(T), 0) << std::endl;
	std::cout << "Somme croissante : ";
	std::cout << somme_croissante(std::vector<double>(T)) << std::endl;
	std::cout << "Somme décroissante : ";
	std::cout << somme_decroissante(std::vector<double>(T)) << std::endl;
	std::cout << "Somme absolue croissante : ";
	std::cout << somme_abs_croissante(std::vector<double>(T)) << std::endl;
	std::cout << "Somme absolue decroissante : ";
	std::cout << somme_abs_decroissante(std::vector<double>(T)) << std::endl;
	std::cout << "Somme itérative : ";
	std::cout << somme_iterative(std::vector<double>(T)) << std::endl;
	
}
