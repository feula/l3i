/*
  donnees_somme.hpp --- Fichier C++ de données pour l'exercice 1.3 du TP 1 
  X5I0030 (Architecture des ordinateurs).
  
 */


#ifndef __donnees_somme_hpp__
#define __donnees_somme_hpp__

#include <vector>
#include <cstdint>

const uint32_t szT = 31;
/*
  S = \sum T = 2.6
*/
const std::vector<double> T { 9007199254740992.0, 999999999999.9, -999999999998.9, 
	 3.56, 12.8766, 0.0123, 999394.4453, 1265356.434536, 23.53, 7889.123, 0.00002145, 
	 0.5, 0.06456, 1254534536.4574, -1254534536.4575, -9007199254740992.0, 
    -999999999999.9, 999999999998.9, -3.56, -12.8766, -0.0123, 
    -999394.4453, -1265356.434536, -23.53, -7889.123, -0.00002145, 
    -0.5, -0.06456, -1254534536.4574, 1254534536.4575, 2.6 };

#endif /* __donnees_somme_hpp__ */
