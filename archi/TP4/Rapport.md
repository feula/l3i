# TP4 - Assembleur

## Exercice 4.1  -- Années bissextiles

Le calendrier actuel (grégorien) définit une année bissextile comme une année multiple de 4, mais pas multiple de 100, sauf si multiple de 400.

Avant 1582, le calendrier en utilisation (julien) définissait toute année multiple de 4 comme bissextile.

```asm
__start:
...
check_calendar:
	blt $t0, 1582, julien
	j gregorien # Pas necessaire, mais bon.

gregorien:	
if1:	rem $t1, $t0, 100
...
else1:
julien:
if2:
	rem $t1, $t0, 4
...
```

## Exercice 4.2

```cpp
#include <iostream>

int main() {
  char t[] = {0,3,2,5,0,3,5,1,4,6,2,4};
  unsigned int jour, mois, annee;
  unsigned int temp;
  std::cout << "Jour?";
  std::cin >> jour;  
  std::cout << "Mois?";
  std::cin >> mois;
  std::cout << "Année?";
  std::cin >> annee;
  
  if (mois < 3) {
    --annee;
  }
  temp = annee;
  unsigned int annee_shift = annee >> 2;
  temp += annee_shift;
  annee_shift = annee/100;
  temp -= annee_shift;
  annee_shift = annee_shift >> 2;
  temp += annee_shift;
  annee_shift = mois -1;
  annee_shift = t[annee_shift];    

  temp += annee_shift;
  temp += jour;
  temp = temp % 7;
  temp = temp << 2;

  char* jour_str[] = {"dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"};
  std::cout << jour_str[temp / 4] << std::endl; 
}
```

