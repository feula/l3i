    .data
valeur:     .word 32768
greater:    .asciiz "Votre chiffre est plus grand.\n"
lower:      .asciiz "Votre chiffre est plus petit.\n"
equal:      .asciiz "Bien ouej cousin\n"
tries:      .asciiz "Nombre d'essais: "
question:   .asciiz "Entrez un nombre: \n"
    .text
	.globl __start

__start:
    li $t0, 0 		# $t0: compteur d'essais
    lw $t2, valeur # $t2: valeur à deviner 
    loop_start:
        la  $a0, question 
        li  $v0, 4
        syscall         # Affichage de la question

        li  $v0, 5
        syscall         # Récupération de l'entrée utilisateur
        move $t1, $v0   # $t1: valeur essayée
        add $t0, $t0, 1 # incrémentation du nombre d'essais
        
        li $v0, 4       # préparation de l'affichage
        beq $t1, $t2, guess_equal
        bgt $t1, $t2, guess_greater
        blt $t1, $t2, guess_lower

        guess_greater:
            la $a0, greater
            syscall
            b loop_start
        guess_lower:
            la $a0, lower
            syscall
            b loop_start
        guess_equal:
            la $a0, equal
            syscall
    
    la $a0, tries
    syscall
    
    li $v0, 1
    move $a0, $t0
    syscall
    