# TD2 - Performances
## Exercice 1

|Programme|Temps sur S1|Temps sur S2|
|:-:|:-:|:-:|
|1|10s|5s
|2|3s|4s

```
Rentabilité S1: 1/10 / 10000 = 0.00001  
Rentabilité S2: 1/5  / 15000 = 0.000013333
```

S2 est plus 1.33 fois plus rentable que S1 sur l'execution du programme 1.

```
Performances S1: (1/10 + 1/3) / 2 = 0.216
Performances S2: (1/5 + 1/4) / 2  = 0.225
Rentabilité S1: PS1 / 10000       = 0.000007692  
Rentabilité S2: PS2 / 15000       = 0.000007407
```

S1 est le plus performant, 1.037 fois plus que S2 des programmes 1 & 2.

```
IPS S1: 20 × 10⁶ / 10s : 2 × 10⁶ (soit 2 MIPS)  
IPS S2: 16 × 10⁶ / 5s : 3.2 × 10⁶ (soit 3.2 MIPS)

Freq S1: 20MHz = 20.000.000 Hz
CPI S1: 10 cycles (20.000.000 / 2.000.000) / instruction
```

