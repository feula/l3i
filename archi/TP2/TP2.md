# TP n° 2 : Logique Combinatoire 

## Simulation d'un circuit
### 1.
1. ![](Xor.png)
2. 
3. Ce circuit est équivalent à une porte XOR. La sortie est à un si et seulement si uniquement une des deux entrées est active.
    
|A|B|S|
|:-:|:-:|:-:|
|0|0|**0**|
|0|1|**1**|
|1|0|**1**|
|1|1|**0**|

4. 
5. 
6. Expression: ~A&B + A&~B   
![](./generated.png)

## Conception d'un circuit

1. Entrées: A(1b), B(1b), S(1b), R<sub>s</sub>(1b)     

|A|B||S|R<sub>s</sub>|
|:-:|:-:|-|:-:|:-:|
|0|0||**0**|**0**|
|0|1||**1**|**0**|
|1|0||**1**|**0**|
|1|1||**0**|**1**|

`S = A XOR B, Rs = A . B` 

![](./add1.png)

2. Entrées: A(1b), B(1b), R<sub>e</sub>(1b), R(1b), R<sub>s</sub>(1b)

|R<sub>e</sub>|B|A||R|R<sub>s</sub>|
|:-:|:-:|:-:|-|:-:|:-:|
|0|0|0||**0**|**0**
|0|0|1||**1**|**0**
|0|1|0||**1**|**0**
|0|1|1||**0**|**1**
|1|0|0||**1**|**0**
|1|0|1||**0**|**1**
|1|1|0||**0**|**1**
|1|1|1||**1**|**1**

![](./add1r.png)

3. Ripple-carry adder. Très lent, car doit attendre le calcul du carry de l'adder précédent.

![](./add4.png)