# X5I0020 - Algorithmes de tri

## Exercice 1

```python
def tri_selection(T, deb, fin):
  if (deb < fin):
      # Recherche de l'élément maximum de T
      k = deb
      for (i in deb..fin):
          if (T[i] > T[k]): k = i
      Echanger(T, k, fin)
      tri_selection(T, deb, fin-1)
```

### 1. Terminaison

### 2. Correction

Comme $C(n)$ est vraie et que $fin -1 + deb + 1 = fin - deb +1 - 1 = n + 1 - 1 = n$
$tri\_selection(T, deb, fin-1)$ retourne dans $T[deb, fin-1]$ le tableau passé en paramètre trié dans l'ordre croissant.
Ceci assure que $T[deb, fin]$ est trié dans l'ordre croissant. Ainsi, $C(n+1)$ est vrai, donc quelque soit $n <= 1$,$C(n)$ est vrai.

On doit prouver que à la fin de la ligne 5, $k$ contient l'indice du plus grand élément du tableau $T[deb, fin]$

$P(n)$: "A la fin de l'execution de 'POUR' lorsque $i = n$, $k$, contient le plus grand élément de tableau $T[deb, k]$"
Montrons $P(deb)$: A la fin de l'éxécution de la ligne 2, $k$ contient $deb$. Comme $fin > deb$ lorsque $i = deb$, on éxécute la boucle POUR.
Comme la condition du SI ligne 4 n'est pas vérifiée, $k$ n'est pas modifié.
A la fin de l'execution du POUR, lorsque $i = deb$, $k$ contient $deb$. Or $deb$ est bien l'indice le plus grand élément de $T[deb, deb]$.

**Donc $P(deb)$ est vraie**.

Soit $n >= deb$ et $n < fin$ tel que $P(n)$ soit vraie.
Montrons $P(n+1)$.
A la fin du "POUR", lorsque $i = deb+1, k$ contient le plus grand élément de $T[deb, deb+1]$.
Comme $P(n)$ est vraie, au début du passage dans la boucle POUR, alors $i = n+1$ (passage possible car $n+1 <= fin$), $k$ contient l'indice du plus grand élément du tableau $T[deb, n]$.

Comme on passe par la ligne 4, on à donc deux possibilités.

* **Soit $T[n+1] > T[k]$:**
  Ainsi $\forall u \in [deb, n],\ T[n+1] > T[u]$, comme la condition est vérifiée, on passe dans le alors et $k$ prend la valeur $n+1$.
  Ce qui assure que $\forall \alpha \in [deb, n+1], T[n+1] > T[\alpha]$ 
  Donc pour ce cas, $k$ a la fin de l'éxécution du POUR lorsque $i = n+1, k$ contient le plus grand élément de $T[deb, n+1]$.
* **Soit $T[n+1] <= T[k]$:**
  Ainsi $\forall u \in [deb, n],\ T[k] \ \geq T[n] $. Comme la condition du SI n'est pas vérifiée, on termine l'éxécution du POUR lorsque $i=n+1$. Ce qui assure que $k$ contient l'indice du plus grand élément du tableau $T[deb, n+1]$

**Ainsi, $P(n+1)$ est vraie, donc $\forall u \in [deb, fin],\ P(u)$ est vraie**.

Les hypothèses de langages nous assurent que le dernier passage dans le POUR a été fait lorsque $i = n$. Au début de la ligne 6, $P(fin)$ est vraie, et donc k contient l'indice du plus grand élément de $T[deb, fin]$.

### 3. Nombre d'appels à échanger

$P(n)$ : $\forall T$ un tableau d'entiers distincts, $\forall deb,fin$ des indices de $T$ tel que $fin + deb + 1 = n$.
$tri\_selection(T, deb, fin)$ **réalise $n-1$ appels à échanger.**

1. $P(1)$ est vraie, en effet la condition du SI de la ligne 1 est vérifiée.
2. Avec $n \geq 1$, $P(n)$ est vraie car on passe dans ALORS, on fait donc un appel à $échanger()$  plus $n-1$ appels à $echanger()$ dans l'appel récursif à $tri\_selection(T, deb, fin-1)$ 

$C(n)$ : $\forall T$ un tableau d'entiers distincts, $\forall deb, fin$  des indices $T$ tel que $fin - deb + 1 = n$.
$tri\_selection(T, deb, fin)$ fait:

* $n : 1 + n + \#(n-1)$
* $n - 1: 1 + n-1 + \#(n-2)$
* ...
* $2: 1 + 2$
* $1: 1$

appels, soit $n + \sum_{i=2}^n$ = $n + \frac{(n+2)(n-1)}{2}$
​                    $1 + \sum_{i=2}^n(1+\sum_{j=1}^i 1)$

### 4. Nombre de comparaisons

$MC(n)$ : $\forall T$ un tableau d'entiers distincts, $\forall deb, fin$ des des indices de $T$.

1. $fin - deb + 1 = u$
2. $T[deb+1, fin]$ trié dans l'ordre croissant
3. $T[deb]$ est le plus grand élément du tableau $T[deb, fin]$

Alors le nombre d'affectations de la variable $k$ est $n-1$

**Montrons $MC(1)$**:

Soit $T$ un tableau d'indices distincts, soit $deb, fin$ des indices de $T$ tel que $fin - deb + 1 = u$.
Comme $fin = deb$ la condition du SI de la ligne 1 n'est pas vérifiée. Donc on ne fait pas aucune affectation de la variable $k$ ce qui assure $MC(1)$ 

Soit $n >= 1$ tel que $MC(n)$  soit vrai. 
**Montrons $MC(n+1)$**: 

Soit $T$ un tableau d'entiers vérifiant les conditions 1,2,3, soit $deb, fin$ des indices de $T$ tel que $fin - deb +1 = n+1$.
Comme $n >= 1$, $fin > deb$, la condition SI de la ligne 1 est donc vérifiée. On passe à la ligne 2 et on a donc une affectation de la variable $k$.

