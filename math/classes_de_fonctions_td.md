# TD - Classes de fonctions
## Exercice 10

$$
\theta(n^b) = \{g : g \in \mathbb{R}^\mathbb{R}\, \exists c_1, c_2 \in \mathbb{R}^{+*}, \exists x_1 \in R, \forall  x \geq x_0;
0 \leq g(n) \leq x_1(n^b)\}\\~\\
$$
$$
g(n) = (n+a)^b \\ 
\forall n \geq a, n+a \leq 2n \\
\forall n \geq max(-a, a)\\
0 \leq n+a \leq 2n \\
donc \ 0 \leq (n+a)^b \leq (2n)^2 \\
or \ 2^n  est \ une \ constante \\
donc \ \exists c_1 \in \mathbb{R^{+*}}, c_1 = 2^b, \exists \ x_0 \in \mathbb{R}, x_0 = |a| \\
\forall n \geq x_0, 0 \leq (n+a)^b \leq c_1(n^b) \\
donc \ (n+a)^b \in O(n^b) \\~\\
O \leq \beta a \leq n + a \\
soit \ \beta \ tel \ que \ 0 \lt \beta \lt 1 \\
on \ a \ donc \ n + a \geq \beta a \\
ainsi \ \exists x_2 \ in \mathbb{R}^(+*), c_2 = \beta^b \\
\exists x_1 \in \mathbb{R}, x_1 = max(0, {a \over{1-\beta}}) \\
\forall n \geq x_0 \\
0 \leq c2(n^b) \\ leq (n+a)^b
$$



## Exercice 11

$$
f(n) = 2^{n+k}
$$

## Exercice 12

$$
Montrez \ que (i) \ lg(n!) \in \theta(n \times lg(n)) \ et \ que \ (ii) \ n! \in O(n^n)
$$

Supposons que $\in O(2^n)$
Donc $\exists c \in \mathbb{R}^{+*}, \exists x_1 \in \mathbb{R}$ tel que $\exists n \geq x_0$
On a $0 \leq 2^n \leq c\times 2^n$ 
​         $log(2^{xn}) \leq log(c \times 2^n)$  
​         $log(2^{xn}) \leq log(c) \times log(2^n)$
​         ${log(2^{xn}) \over{log(2^n)}} \leq log(c)$
​          $x^n \leq c$

Pour $n \geq max(log(c), x_0)$, contradiction

## Exercice 15

$$
1. \ f \in O(g) \implies g \in \Omega(f)
$$

​           $f \in O(g) \iff \exists c_1 \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R}, \forall x \geq x_0; 0 \leq f(x) \leq c_1 \times g(x)$
$\implies g\in\Omega(f) \iff \exists c_2 \in \mathbb{R}^*_+, \exists x_2 \in \mathbb{R}, \forall x \geq x_1; 0 \leq c_2 \times g(x) \leq g(x)$
$$
c_1 = c_2
$$

$$
2. \ f \in O(g) \implies g \in \Omega(f)
$$

Soit $f(x) = x$, $g(x) = x^2$
$x \in O(x^2) \not \implies x^2 \in O(x)$

Voir ex 4.ii
$$
3. \ f+g\in \Theta(min(f, g))
$$
$f+g \in O(min(f, g)), f+g \in \Omega(min(f, g))$
Soit $f(x) = x$ et $g(x) = x^2$
En effet, pour $x \geq 1$, on a $min(f, g)(x) = f(x)$
Or, on a montré dans 2) que $x^2 \notin O(x)$
$$
4. \ \forall f \in \mathbb{R}^\mathbb{R}, \exists c \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R} \ tel \ que \ \forall x \in \mathbb{R}, \ avec \ x_0 \leq c \times f(\frac{x}{2})
$$
Non,en effet, par exemple lorsque $f(x) = 2^{2x}$, on se retrouve dans le cas de l'exercice $11.ii$
$$
5. \ \forall h \in o(f), f+h\in\Theta(f)
$$
Si $h\in o(f)$, alors $\forall c \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R}$ tel que $x_0 \leq x$, avec $0 \leq h(x) \leq c \times f(x)$ 
**Montrons que $f+h\in O(f)$**
Soit $c \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R}$ tel que $\forall x \geq x_0$, on a $0 \leq h(x) \leq c \times f(x)$
Donc, $0 \leq (f+h)(x) \leq (c+10) \times f(x)$
Ce qui assure que $(f+g) \in \Theta (f)$

**Montrons que $f+h \in \Theta (f)$**
Comme $h \in o(f)$, $h$ et $f$ sont asymptotiquement $\geq 0$ 
Donc $\forall x_0 \in \mathbb{R}$ tel que $x \geq x_0, h (h) \geq 0, f(x) \geq 0$
Ce qui assure que $f+h \in \Omega(f)$
Donc $f+h \in \Theta(f)$


$f \in \Theta(g) \iff g \in \Theta(f)$
$$
6.\ f \in O((f)^2)
$$
**Montrons que pour $f(x) = \frac{1}{x}, f \notin O((f)^2)$**
Supposons $f(x) \in O((f(x))^2)$
Donc $\exists c \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R}, \forall x \geq x_0, 0 \leq f(x) \leq c \times f(x)^2$
C'est à dire $\forall x \geq x_0, 0 \leq \frac{1}{x} \leq \frac{1}{x^2}$
Contradiction avec $x \gt max(x_0, c)$
$$
7.\ f \in O(f^2),\ lorsque\ f^2 = f\ o\ f
$$
Supposons $f$ asymptotiquement positif à partir de 1
Donc $\exists x_0, \forall x \geq x_0, f(x) \geq 1$
C'est à dire $f^2(x) \geq f(x) \geq 0$
Donc $f \in O(f^2)$

$f \in O(f\ o\ f)$
Si $f$ est extensive cela marche
$f$ est extensive si $\forall x \in D_f, x \leq f(x)$
**Montrons que si $f = 2n, f \notin O(f\ o\ f)$**
**Preuve 1**
Soit $x \in \mathbb{R^+}, 0 \leq x \leq f(x)$
​	Or $f(x) \leq f(f(x))​$ donc $0 \leq f(x) \leq f^2(x)​$
​	Donc $f \in O(f^2(x))$

**Preuve 2**
Supposons que $ln \in O(ln(ln)$
​	$\exists x \in \mathbb{R}^*_+, \exists x_0 \in \mathbb{R}, \forall x \geq x_0, 0\leq ln(x) \leq ln(ln(x))$
​	donc $x \leq e^{x \times ln(ln(x))}$ c'est à dire $x \leq ln(x)^e$
​	c'est à dire $x^{\frac{1}{2}} \leq ln(x)$ ce que contredit $\lim_{x \to \infty} \frac{ln(x)}{x^r} = 0,\ \forall r \gt 0$ 
$$
8.\ f \in O(g) \implies 2^{f(.)} \in O(2^{g(.)})
$$
Voir exercice $11.ii$

