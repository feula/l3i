$$
\huge{Etude\ \ des\ \ algorithmes}\\
\LARGE{Devoir\ Maison\ n° 1}\\
------------------------\\
HERAUD\ Xavier\\PHALAVANDISHVILI\ Demetre\\
EULA\ Florian
$$

### Le problème $CNF-SAT$

**Montrons que $CNF-SAT$ est dans $NP$**
Trouvons un certificat polynômial pour $CNF-SAT$. Soit $C$ une instance de $CNF-SAT$. Notons $V(C)$ l'ensemble des variables par $C$. Le certificat polynômial est une application $F$ de $V(C)$ dans l'ensemble $\{0, 1\}$ est un assignement. En effet:

1. $\#F \in O(\#V(C))​$ donc $\#F \in O(\#C)​$ , c'est à dire que $F​$ est donc de taille polynômiale.
2. Montrons que l'on dispose d'un algorithme polynômial en $\#F$ et $\#C$ qui nous permet de vérifier que $F(C) = 1$. En effet, on peut obtenir à l'aide d'un algorithme polynômial en $\#C$ son arbre de décomposition.Il suffit alors d'utiliser $F$ pour assigner à chacune des feuilles de cet arbre la valeur induite par $F$. Ceci peut être fait avec un algorithme polynômial en évaluant les noeuds "ET" et "OU"

**$NegLit-SAT$**
En utilisant la réduction évoquée en travaux dirigés, notons la $\phi$ dans ce qui suit, montrons par induction que pour toute instance $F$ de $CNF-SAT$:

1. $F$ est satisfiable si et seulement si $\phi(F)$
2. $\phi(F)$ est de taille polynômiale en celle de $F$