# BDD - TP2 - Reporting
**Florian Eula**

------------------------

#### 1. Séquences
```sql
CREATE SEQUENCE seq_idcl START WITH 1 INCREMENT BY 1;
```

#### 2. Génération
```sql
SELECT 'DELETE FROM ' || table_name || ';'
FROM USER_TABLES;
```
```
'DELETEFROM'||TABLE_NAME||';'
-------------------------------------------
DELETE FROM CLIENTS;
DELETE FROM LIVRES;
DELETE FROM ACHATS;
DELETE FROM AVIS;
```
La requete nous permet de générer une chaine de caractères, dans laquelle on concatène le nom de toutes les tables créées par l'utilisateur.

#### 3. Spool
```sql
SET ECHO OFF
SPOOL effacer.sql
SET ECHO OFF
SET FEEDBACK OFF
SET HEADING OFF
SET PAGESIZE 0
-- Données à écrire dans le fichier
SELECT 'SELECT * FROM ' || table_name || ';' FROM USER_TABLES;
--
SPOOL OFF
SET ECHO ON
```
A noter que lorsque cela est éxécuté via sqlplus, le prompt `SQL>` est également écrit dans le fichier. C'est génant.
Certaines tables ne sont pas vidées car cela violerait des contraintes (notamment de clés étrangères.)

#### 4. Autre exemple
Affichage du contenu de toutes les tables  
```sql
SELECT 'SELECT * FROM ' || table_name || ';' FROM USER_TABLES;
```

#### 5. Ajout du prix
```sql
ALTER TABLE Achats ADD prix numeric;
```

#### 6. Rapport
```sql
SET ECHO OFF
SPOOL 2013-01-25-achats.lst
SET ECHO OFF
SET FEEDBACK OFF
SET PAGESIZE 100
SET HEADING ON
SET headsep !
ttitle 'Achats des clients au ' _DATE
column idcl format a10
column idcl heading 'IDCL'
column dateachat format a20 word_wrapped
column dateachat heading 'Date achat'
column genre format a20
column genre heading 'Genre'
column prix format 99.99
column prix heading 'Prix'
break on idcl skip 1 on report
compute avg sum of prix on idcl

-- Données à écrire
SELECT to_char(idcl) as idcl, dateachat, upper(genre) as genre, prix FROM Achats
LEFT JOIN Livres on Achats.refl = Livres.refl
ORDER BY idcl, dateachat;
--
SPOOL OFF
SET ECHO ON

```

**Sortie:**
```
Achats des clients au 20-JAN-17                                                 
IDCL       Date achat           Genre                  Prix                     
---------- -------------------- -------------------- ------                     
0          12-JAN-17            SCIENCE-FICTION       22.00                     
           13-JAN-17            HISTOIRE VRAIE        33.00                     
**********                                           ------                     
avg                                                   27.50                     
sum                                                   55.00                     
                                                                                
1          03-JAN-70            COLONIALISME          42.00                     
**********                                           ------                     
avg                                                   42.00                     
sum                                                   42.00                     
                                                                                
2          03-JAN-70            COLONIALISME          23.00                     
**********                                           ------                     
avg                                                   23.00                     
sum                                                   23.00                     
```