--@fonctions_procedures.sql

set serveroutput on;

CREATE OR REPLACE PROCEDURE pr_maj_dateFinTache (nomT TacheProjet.nomTache%type, nomP TacheProjet.nomProjet%type, dateFinT TacheProjet.dateFinTache%type) 
IS
	nbTaches NUMBER;
BEGIN
	UPDATE TacheProjet SET dateFinTache = dateFinT WHERE nomTache = nomT and nomProjet = nomP;
	SELECT COUNT(nomTache) into nbTaches FROM TacheProjet WHERE nomProjet = nomP and dateFinTache != NULL;
	IF nbTaches = 0
	THEN 
        UPDATE Projet SET dateFin = dateFinT, nbVentes = 0 WHERE nomProjet = nomP;
	END IF;
END pr_maj_dateFinTache;
/


CREATE OR REPLACE PROCEDURE pr_annulation_projet (nomP Projet.nomProjet%type) 
IS
	erreur_annulation EXCEPTION;
	dateF Projet.dateFin%type;
BEGIN
	SELECT dateFin into dateF FROM Projet WHERE nomProjet = nomP;
	IF dateF IS NULL
	THEN DELETE FROM Projet WHERE nomProjet = nomP;
	ELSE raise erreur_annulation;
	END IF;
	EXCEPTION
	WHEN erreur_annulation THEN
		RAISE_APPLICATION_ERROR(-20009,'le projet est déjà fini');
			
END pr_annulation_projet;
/


CREATE OR REPLACE PROCEDURE pr_vente_projet (nomP Projet.nomProjet%type, nbV Projet.nbVentes%type) 
IS
BEGIN
	UPDATE Projet SET nbVentes = nbVentes + nbV WHERE nomProjet = nomP;
END pr_vente_projet;
/


CREATE OR REPLACE PROCEDURE pr_calcul_profit (nomP Projet.nomProjet%type) 
IS
	profit NUMBER;
	nbVente NUMBER;
	prix NUMBER;
BEGIN
	SELECT nbVentes into nbVente FROM Projet WHERE nomProjet = nomP;
	SELECT prixUnitaire into prix FROM Projet WHERE nomProjet = nomP;
	profit := nbVente*prix;
	dbms_output.put_line(profit);
END pr_calcul_profit;
/


-- choix = 1 pour ajouter et n'importe quoi d'autre pour soustraire
CREATE OR REPLACE PROCEDURE pr_maj_salaire (idCompetence Salaires.idCompetenceEmpl%type, val NUMBER, choix NUMBER) 
IS
	CURSOR C2 IS SELECT * FROM Salaires WHERE idCompetenceEmpl = idCompetence ORDER BY niveauCompetence DESC;
	CURSOR C3 IS SELECT * FROM Salaires WHERE idCompetenceEmpl = idCompetence ORDER BY niveauCompetence ASC;
BEGIN
	IF choix = 1
	THEN
		FOR ligne in C2 LOOP
		UPDATE Salaires SET salaire = salaire + val WHERE idCompetenceEmpl =idCompetence and niveauCompetence = ligne.niveauCompetence;
		END LOOP;
	ELSE
		FOR ligne in C3 LOOP
		UPDATE Salaires SET salaire = salaire - val WHERE idCompetenceEmpl =idCompetence and niveauCompetence = ligne.niveauCompetence;
		END LOOP;
	END IF;
END pr_maj_salaire;
/
