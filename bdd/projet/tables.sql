-- @tables.sql

DROP TABLE Competences CASCADE CONSTRAINT;
DROP TABLE Salaires CASCADE CONSTRAINT;
DROP TABLE Employe CASCADE CONSTRAINT;
DROP TABLE Projet CASCADE CONSTRAINT;
DROP TABLE Tache CASCADE CONSTRAINT;
DROP TABLE TacheProjet CASCADE CONSTRAINT;
DROP TABLE AssignationEmploye CASCADE CONSTRAINT;
DROP SEQUENCE Seq_Competences_idComp;
DROP SEQUENCE Seq_Employe_IdEmpl;

-- On crée une table qui regroupe les compétences car les comp des employes et des taches sont les mêmes

CREATE Table Competences
(
	idCompetence NUMBER NOT NULL,
	nomCompetence VARCHAR2(30) NOT NULL,
	PRIMARY KEY (idCompetence)
);

CREATE SEQUENCE Seq_Competences_idComp START WITH 0 MINVALUE 0 INCREMENT BY 1;

CREATE TABLE Salaires
(
	idCompetenceEmpl NUMBER NOT NULL,
	niveauCompetence NUMBER NOT NULL,
	salaire NUMBER NOT NULL,
	PRIMARY KEY (idCompetenceEmpl, niveauCompetence),
	CONSTRAINT fk_Salaires_idCompetence FOREIGN KEY (idCompetenceEmpl) REFERENCES Competences(idCompetence),
	CONSTRAINT check_Salaires_nivComp CHECK (niveauCompetence BETWEEN 0 and 10),
	CONSTRAINT check_Salaires_salaire CHECK (salaire >= 0)
);

CREATE TABLE Employe
(
	idEmpl NUMBER NOT NULL,
	nomEmpl VARCHAR2(30) NOT NULL,
	prenomEmpl VARCHAR2(30) NOT NULL,
	idCompetenceEmpl NUMBER NOT NULL,
	niveauCompetence NUMBER NOT NULL,
	PRIMARY KEY (idEmpl),
	CONSTRAINT fk_employe_idCompetenceEmpl FOREIGN KEY (idCompetenceEmpl,niveauCompetence) REFERENCES Salaires(idCompetenceEmpl,niveauCompetence)
);

CREATE SEQUENCE Seq_Employe_IdEmpl START WITH 0  MINVALUE 0 INCREMENT BY 1;

-- Tables projet société de création de jeux vidéos

CREATE TABLE Projet
(
	nomProjet VARCHAR2(30) NOT NULL,
	dateDebut DATE NOT NULL,
	dateFin DATE,
	chefProjet NUMBER NOT NULL,
	typeProjet VARCHAR2(30) NOT NULL,
	prixUnitaire NUMERIC(4,02) NOT NULL,
	nbVentes INTEGER,
	moteurGraphique VARCHAR2(30),
	design VARCHAR2(30),
	licence VARCHAR2(30),
	gameplay VARCHAR2(30),
	sons VARCHAR2(30),
	PRIMARY KEY (nomProjet),
	CONSTRAINT fk_projet_chefProjet FOREIGN KEY (chefProjet) REFERENCES Employe(idEmpl)
);

CREATE TABLE Tache
(
	nomTache VARCHAR2(30) NOT NULL,
	idCompetenceTache NUMBER NOT NULL,
	PRIMARY kEY (nomTache),
	CONSTRAINT fk_tache_competenceTache FOREIGN KEY (idCompetenceTache) REFERENCES Competences(idCompetence)
);

CREATE TABLE TacheProjet
(
	nomTache VARCHAR2(30) NOT NULL,
	nomProjet VARCHAR2(30) NOT NULL,
	dateDebutTache DATE NOT NULL,
	dateFinTache DATE, 
	PRIMARY KEY (nomTache, nomProjet),
	CONSTRAINT fk_tacheProjet_nomTache FOREIGN KEY (nomTache) REFERENCES Tache(nomTache),
	CONSTRAINT fk_tacheProjet_nomProjet FOREIGN KEY (nomProjet) REFERENCES Projet(nomProjet)
);

CREATE TABLE AssignationEmploye
(
	idEmpl NUMBER NOT NULL,
	nomTache VARCHAR2(30) NOT NULL,
	nomProjet VARCHAR2(30) NOT NULL,
	PRIMARY KEY (idEmpl, nomTache, nomProjet),
	CONSTRAINT fk_assignationEmploye_idEmpl FOREIGN KEY (idEmpl) REFERENCES Employe(idEmpl),
	CONSTRAINT fk_assignEmpl_nomT_nomP FOREIGN KEY (nomTache, nomProjet) REFERENCES TacheProjet(nomTache, nomProjet)
);











