--Jeux d'essais

	prompt ********************************** jeux d essais **********************************;
	
	--inserts préalables
	prompt ******************************** inserts préalables *******************************;
	
	INSERT INTO Competences VALUES (100, 'Game Designer');
	INSERT INTO Competences VALUES (200, 'Sound Designer');
	INSERT INTO Competences VALUES (300, 'Musique Zone1');

	INSERT INTO Salaires VALUES (100, 3, 10000);

	INSERT INTO Employe VALUES (100, 'Ancel', 'Michel', 100, 3);
	INSERT INTO Employe VALUES (200, 'José', 'Bové', 100, 3);

	INSERT INTO Tache VALUES ('Design Zone1', 100);
	INSERT INTO Tache VALUES ('Design Zone2', 100);
	INSERT INTO Tache VALUES ('Design Zone3', 100);
	INSERT INTO Tache VALUES ('Design Zone4', 100);
	INSERT INTO Tache VALUES ('Design Zone5', 100);
	INSERT INTO Tache VALUES ('Design Zone6', 100);
	INSERT INTO Tache VALUES ('Modele Rayman', 100);

	INSERT INTO Projet VALUES ('Rayman 4', '01-MAR-2017',null, 100, 'Plateformer', 40, null, null, null, null, null, null);
		INSERT INTO Projet VALUES ('Wew Lad', '01-MAR-2017',null, 100, 'Plateformer', 40, null, null, null, null, null, null);
	INSERT INTO Projet VALUES ('Moteur1', '01-MAR-2017',null, 100, 'moteurGraphique', 0, null, null, null, null, null, null);
	UPDATE Projet 
	SET DateFin = '19-MAR-2017'
	WHERE NomProjet = 'Moteur1';

	INSERT INTO TacheProjet VALUES('Modele Rayman', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone1', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone2', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone3', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone4', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone5', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO TacheProjet VALUES('Design Zone6', 'Rayman 4', '05-MAR-2017', null);

	prompt ***********************************************************************************;
	

	------------------
	prompt ************************ erreur sur tr_tacheProjet_insert  ************************;
	
	--erreur sur tr_tacheProjet_insert 
	INSERT INTO TacheProjet VALUES ('Modele Rayman', 'Rayman 4', '05-MAR-2017', '17-MAR-2017');
	
	prompt ***********************************************************************************;
	

	prompt ************************ erreur sur tr_tacheProjet_update  ************************;
	
	--erreur sur tr_tacheProjet_update
	UPDATE TacheProjet
	SET dateFinTache = '01-MAR-2017'
	WHERE nomTache = 'Modele Rayman' AND nomProjet = 'Rayman 4';
	
	prompt ***********************************************************************************;


	--tr_assignationEmpl_taches 
	prompt ************************ tr_assignationEmpl_taches ********************************;
	
	
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone1', 'Rayman 4');
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone2', 'Rayman 4');
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone3', 'Rayman 4');
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone4', 'Rayman 4');
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone5', 'Rayman 4');

	prompt ****** erreur: employé déjà assigné à plus de 5 taches *****
	INSERT INTO AssignationEmploye VALUES (100, 'Design Zone6', 'Rayman 4');
	
	prompt ***********************************************************************************;


	--tr_assignationEmpl_competences
	prompt ************************ tr_assignationEmpl_competences ***************************;
	
	INSERT INTO Tache VALUES ('Musique Zone1', 100);
	INSERT INTO TacheProjet VALUES('Musique Zone1', 'Rayman 4', '05-MAR-2017', null);
	INSERT INTO AssignationEmploye VALUES (200, 'Musique Zone1', 'Rayman 4');
	
	prompt ***********************************************************************************;
	

	--tr_Projet_dateFin_nbVentes
	prompt ************************ tr_Projet_dateFin_nbVentes *******************************;
	
		--erreur sur date_fin != null
	prompt ************************ erreur sur date_fin != null ******************************;
	
	INSERT INTO Projet VALUES ('Rayman 4', '01-MAR-2017', '02-MAR-2017', 200,'Plateformer', 40, null, 'Moteur1', 'Design1', 'Rayman', 'Gameplay1','Sons1');
	
	prompt ***********************************************************************************;
	
	
		--erreur sur nbVentes != null
	prompt ************************ erreur sur nbVentes != null ******************************;
	
	INSERT INTO Projet VALUES ('Rayman 5', '01-MAR-2017', null, 100,'Plateformer', 40, 1000000, 'Moteur1', 'Design1', 'Rayman', 'Gameplay1','Sons1');
	
	prompt ***********************************************************************************;


	--tr_Projet_moteurGraphique
	prompt ************************ erreur sur tr_Projet_moteurGraphique ********************************;
	
	INSERT INTO Projet VALUES ('Rayman 6', '01-MAR-2017', null, 100,'Plateformer', 40, null, 'Moteur pas fini', 'Design1', 'Rayman', 'Gameplay1','Sons1');
	
	prompt ***********************************************************************************;


	--tr_Salaires_salaire
	prompt ************************ tr_Salaires_salaire **************************************;
	
		--pas d'erreur à cet insert
	prompt ************************ pas d erreur à cet insert ********************************;
	
	INSERT INTO Salaires VALUES (100, 2, 10000);
	
	prompt ***********************************************************************************;
	

		--erreur : salaire trop élevé par rapport à une comp de même type lvl 2
	prompt ****** erreur : salaire trop élevé par rapport à une comp de même type lvl 2 ******;
	
	INSERT INTO Salaires VALUES (100, 1, 15000);
	
	prompt ***********************************************************************************;
	

		--erreur : salaire trop bas par rapport à une comp de même type lvl 2
	prompt ****** erreur : salaire trop bas par rapport à une comp de même type lvl 2 ********;
	INSERT INTO Salaires VALUES (100, 3, 1000);
	
	prompt ****** [Test] Procédures ********;
	prompt ****** MAJ Date de Fin ********;
	EXEC pr_maj_dateFinTache ('Musique Zone1', 'Rayman 4', '04-MAR-2019');

	prompt ****** MAJ Ventes projet ********;
	EXEC pr_vente_projet('Rayman 4', 2000000);

	prompt ****** Calcul profit ********;
	EXEC pr_calcul_profit('Rayman 4');

	prompt ****** MAJ salaire ********;
	EXEC pr_maj_salaire(1, 200, 1);
	prompt ****** Annulation d''un Projet, echec ********;
	EXEC pr_annulation_projet('Rayman 4');

	prompt ****** Annulation d''un Projet ********;
	EXEC pr_annulation_projet('Wew Lad');
	prompt ***********************************************************************************;
