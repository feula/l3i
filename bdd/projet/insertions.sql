-- @insertions.sql

-- Competences(idCompetence, nomCompetence)

INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, 'Code');
INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, 'Design');
INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, 'Gestion');
INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, 'Musique');
INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, 'Ecriture');
-- INSERT INTO Competences VALUES (Seq_Competences_idComp.nextval, '');

-- Salaires(idCompetenceEmpl, niveauCompetence, salaire)
INSERT INTO Salaires VALUES (1, 1, 600);
INSERT INTO Salaires VALUES (1, 2, 900);
INSERT INTO Salaires VALUES (1, 3, 1500);
INSERT INTO Salaires VALUES (1, 5, 3000);
INSERT INTO Salaires VALUES (1, 7, 14000);

INSERT INTO Salaires VALUES (2, 2, 1200);
INSERT INTO Salaires VALUES (2, 3, 1600);
INSERT INTO Salaires VALUES (2, 4, 2000);
INSERT INTO Salaires VALUES (2, 6, 7000);

INSERT INTO Salaires VALUES (3, 2, 1150);
INSERT INTO Salaires VALUES (3, 7, 16000);
INSERT INTO Salaires VALUES (3, 8, 17000);

INSERT INTO Salaires VALUES (4, 4, 1600);
INSERT INTO Salaires VALUES (4, 5, 3000);
INSERT INTO Salaires VALUES (4, 9, 18000);

INSERT INTO Salaires VALUES (5, 1, 200);
INSERT INTO Salaires VALUES (5, 3, 800);
INSERT INTO Salaires VALUES (5, 4, 1300);
-- INSERT INTO Salaires VALUES (, , );

-- Employe(idEmpl, nomEmpl, prenomEmpl, idCompetenceEmpl, niveauCompetence)

INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'José', 'Gérard', 1, 3);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Touvabien', 'Jean', 1, 3);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Klabnik', 'Steve', 2, 4);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Bob', 'Blédard', 3, 8);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Whiters', 'Bill', 5, 1);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Reinhardt', 'Django', 4, 9);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Collins', 'Phil', 3, 7);
INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, 'Dre', 'Docteur', 1, 7);
-- INSERT INTO Employe VALUES (Seq_Employe_IdEmpl.nextval, '', '', , );





-- Projet(nomProjet, dateDebut, dateFin, chefProjet, typeProjet, prixUnitaire, nbVentes, moteurGraphique, design, licence, gameplay, sons)

-- Création du moteur graphique principal
INSERT INTO Projet VALUES ('Unreal Engine', to_date('01/01/1969' , 'dd/mm/yyyy'), null, 4, 'moteurGraphique', 59.99, null, null, '3D', 'Criminals', 'FPS', 'Dolby 5.1');
-- Création d'un deuxième, pour le lol
INSERT INTO Projet VALUES ('Unity', to_date('01/01/1969' , 'dd/mm/yyyy'), null, 4, 'moteurGraphique', 59.99, null, null, '3D', 'Criminals', 'FPS', 'Dolby 5.1');
INSERT INTO Tache VALUES ('Dev UE', 1);
INSERT INTO Tache VALUES ('Dev Unity', 1);

INSERT INTO TacheProjet VALUES ('Dev UE', 'Unreal Engine', to_date('03/01/1969' , 'dd/mm/yyyy'), null);
--to_date('03/04/1970' , 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Dev Unity', 'Unity', to_date('03/01/1969' , 'dd/mm/yyyy'), null);
EXEC pr_maj_dateFinTache ('Dev UE', 'Unreal Engine', to_date('04/01/1969' , 'dd/mm/yyyy'));
EXEC pr_maj_dateFinTache ('Dev Unity', 'Unity', to_date('04/01/1969' , 'dd/mm/yyyy'));


INSERT INTO Projet VALUES ('Smooth Criminal', to_date('01/01/1970' , 'dd/mm/yyyy'), null, 4, 'Contrat', 59.99, null, 'Unreal Engine', '3D', 'Criminals', 'FPS', 'Dolby 5.1');
--to_date('24/06/1972' , 'dd/mm/yyyy')
INSERT INTO Projet VALUES ('Ain''t no Sunshine', to_date('05/02/2007' , 'dd/mm/yyyy'), null, 7, 'Interne', 19.99, null, 'Unity', '2D', 'Sunshine', 'Platformer', 'Dolby 5.1');
--to_date('19/09/2012' , 'dd/mm/yyyy')
INSERT INTO Projet VALUES ('Stylo', to_date('08/02/2007' , 'dd/mm/yyyy'), null, 7, 'Contrat', 59.99, null, 'Unity', '2D', 'Stylo', 'Educatif', 'Dolby 5.1');
--to_date('26/04/2009' , 'dd/mm/yyyy')
-- INSERT INTO Projet VALUES ('', , , , '', , , '', '', '', '', '');



-- Tache(nomTache, competenceTache)

INSERT INTO Tache VALUES ('Réaliser la bande sonnore', 4);
INSERT INTO Tache VALUES ('Création des modèles', 2);
INSERT INTO Tache VALUES ('Animations des modèles', 2);
INSERT INTO Tache VALUES ('Développer la physique', 1);
INSERT INTO Tache VALUES ('Création de l''IA', 1);
INSERT INTO Tache VALUES ('Ajout des textures', 2);
INSERT INTO Tache VALUES ('Création des dialogues', 5);
INSERT INTO Tache VALUES ('Création des quetes', 5);
INSERT INTO Tache VALUES ('Programmation des scripts', 1);
INSERT INTO Tache VALUES ('LevelDesign', 2);
INSERT INTO Tache VALUES ('Création de l''interface', 2);
INSERT INTO Tache VALUES ('Traduction', 5);
INSERT INTO Tache VALUES ('Communication', 3);
INSERT INTO Tache VALUES ('Marketing', 3);
INSERT INTO Tache VALUES ('Création des sons d''ambiance', 4);
-- INSERT INTO Tache VALUES ('', );



-- TacheProjet(nomTache, nomProjet, dateDebutTache, dateFinTache)

INSERT INTO TacheProjet VALUES ('Création des modèles', 'Smooth Criminal', to_date('03/01/1970' , 'dd/mm/yyyy'), null);
--to_date('03/04/1970' , 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Animations des modèles', 'Smooth Criminal', to_date('06/04/1970' , 'dd/mm/yyyy'), null);
--to_date('27/06/1970' , 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Ajout des textures', 'Smooth Criminal', to_date('24/08/1970' , 'dd/mm/yyyy'), null);
--to_date('09/12/1970' , 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Développer la physique', 'Smooth Criminal', to_date('03/01/1970' , 'dd/mm/yyyy'),null);
-- to_date('16/03/1970' , 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création de l''interface', 'Smooth Criminal', to_date('03/01/1971', 'dd/mm/yyyy'), null);
--to_date('29/09/1971', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('LevelDesign', 'Smooth Criminal', to_date('26/10/1971', 'dd/mm/yyyy'), null);
--to_date('12/04/1972', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des quetes', 'Smooth Criminal', to_date('06/01/1971', 'dd/mm/yyyy'), null);
--to_date('30/10/1971', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des sons d''ambiance', 'Smooth Criminal', to_date('25/03/1971', 'dd/mm/yyyy'), null);
--to_date('11/04/1972', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création de l''IA', 'Smooth Criminal', to_date('14/03/1971', 'dd/mm/yyyy'), null);
--to_date('26/04/1972', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Traduction', 'Smooth Criminal', to_date('12/03/1971', 'dd/mm/yyyy'), null);
--to_date('13/05/1972', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Développer la physique', 'Ain''t no Sunshine', to_date('15/02/2007', 'dd/mm/yyyy'), null);
--to_date('24/03/2008', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Animations des modèles', 'Ain''t no Sunshine', to_date('02/04/2008', 'dd/mm/yyyy'), null);
--to_date('05/01/2009', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des quetes', 'Ain''t no Sunshine', to_date('23/03/2008', 'dd/mm/yyyy'), null);
--to_date('11/10/2011', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des dialogues', 'Ain''t no Sunshine', to_date('11/09/2009', 'dd/mm/yyyy'), null);
--to_date('13/05/2012', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('LevelDesign', 'Ain''t no Sunshine', to_date('06/01/2009', 'dd/mm/yyyy'), null);
--to_date('24/06/2012', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Réaliser la bande sonnore', 'Ain''t no Sunshine', to_date('05/01/2009', 'dd/mm/yyyy'), null);
--to_date('22/06/2012', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Marketing', 'Ain''t no Sunshine', to_date('03/01/2012', 'dd/mm/yyyy'),null);
--to_date('19/09/2012', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des modèles', 'Stylo', to_date('14/02/2007', 'dd/mm/yyyy'), null);
--to_date('26/06/2007', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création de l''interface', 'Stylo', to_date('03/09/2007', 'dd/mm/yyyy'), null);
--to_date('13/06/2008', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Ajout des textures', 'Stylo', to_date('16/09/2008', 'dd/mm/yyyy'), null);
--to_date('25/02/2009', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des sons d''ambiance', 'Stylo', to_date('07/05/2007', 'dd/mm/yyyy'), null);
--to_date('30/11/2008', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('LevelDesign', 'Stylo', to_date('26/03/2007', 'dd/mm/yyyy'), null);
--to_date('29/10/2008', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Création des dialogues', 'Stylo', to_date('02/05/2007', 'dd/mm/yyyy'),null);
--to_date('14/03/2008', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Traduction', 'Stylo', to_date('23/03/2008', 'dd/mm/yyyy'), null);
--to_date('14/02/2009', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Programmation des scripts', 'Stylo', to_date('19/02/2008', 'dd/mm/yyyy'),null);
-- to_date('28/01/2009', 'dd/mm/yyyy')
INSERT INTO TacheProjet VALUES ('Communication', 'Stylo', to_date('14/09/2008', 'dd/mm/yyyy'),null);
-- to_date('26/04/2009', 'dd/mm/yyyy')
-- INSERT INTO TacheProjet VALUES ('', '', , );



-- AssignationEmploye(idEmpl, nomTache, nomProjet)

INSERT INTO AssignationEmploye VALUES (3, 'Création des modèles', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (3, 'Animations des modèles', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (3, 'Ajout des textures', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (1, 'Développer la physique', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (3, 'Création de l''interface', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (3, 'LevelDesign', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (5, 'Création des quetes', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (6, 'Création des sons d''ambiance', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (2, 'Création de l''IA', 'Smooth Criminal');
INSERT INTO AssignationEmploye VALUES (5, 'Traduction', 'Smooth Criminal');

INSERT INTO AssignationEmploye VALUES (1, 'Développer la physique', 'Ain''t no Sunshine');
--INSERT INTO AssignationEmploye VALUES (3, 'Animations des modèles', 'Ain''t no Sunshine');
INSERT INTO AssignationEmploye VALUES (5, 'Création des quetes', 'Ain''t no Sunshine');
INSERT INTO AssignationEmploye VALUES (5, 'Création des dialogues', 'Ain''t no Sunshine');
--INSERT INTO AssignationEmploye VALUES (3, 'LevelDesign', 'Ain''t no Sunshine');
INSERT INTO AssignationEmploye VALUES (6, 'Réaliser la bande sonnore', 'Ain''t no Sunshine');
INSERT INTO AssignationEmploye VALUES (7, 'Marketing', 'Ain''t no Sunshine');

--INSERT INTO AssignationEmploye VALUES (3, 'Création des modèles', 'Stylo');
--INSERT INTO AssignationEmploye VALUES (3, 'Création de l''interface', 'Stylo');
--INSERT INTO AssignationEmploye VALUES (3, 'Ajout des textures', 'Stylo');
INSERT INTO AssignationEmploye VALUES (6, 'Création des sons d''ambiance', 'Stylo');
--INSERT INTO AssignationEmploye VALUES (3, 'LevelDesign', 'Stylo');
INSERT INTO AssignationEmploye VALUES (5, 'Création des dialogues', 'Stylo');
--INSERT INTO AssignationEmploye VALUES (5, 'Traduction', 'Stylo');
INSERT INTO AssignationEmploye VALUES (2, 'Programmation des scripts', 'Stylo');
INSERT INTO AssignationEmploye VALUES (7, 'Communication', 'Stylo');
-- INSERT INTO AssignationEmploye VALUES (, '', '');




