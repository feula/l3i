--@attribution_des_droits_aux_roles.sql
-- ALTER DELETE INSERT SELECT UPDATE

--directeur
GRANT INSERT UPDATE on Competences TO L3_46_directeur;
GRANT SELECT on Competences TO L3_46_directeur;
GRANT INSERT SELECT UPDATE on Salaires TO L3_46_directeur;
GRANT INSERT SELECT UPDATE on Employe TO L3_46_directeur;
GRANT INSERT SELECT on Projet TO L3_46_directeur;
GRANT UPDATE on Tache TO L3_46_directeur;
GRANT INSERT SELECT on Tache TO L3_46_directeur;
GRANT SELECT on TacheProjet TO L3_46_directeur;
GRANT SELECT on AssignationEmploye TO L3_46_directeur;

GRANT EXECUTE on pr_annulation_projet TO L3_46_directeur;
GRANT EXECUTE on pr_vente_projet TO L3_46_directeur;
GRANT EXECUTE on pr_calcul_profit TO L3_46_directeur;
GRANT EXECUTE on pr_maj_salaire TO L3_46_directeur;

--chef de projet
GRANT SELECT on Competences TO L3_46_chefDeProjet;
GRANT SELECT on Employe TO L3_46_chefDeProjet;
GRANT SELECT on Projet TO L3_46_chefDeProjet;
GRANT INSERT SELECT on Tache TO L3_46_chefDeProjet;
GRANT INSERT SELECT on TacheProjet TO L3_46_chefDeProjet;
GRANT EXECUTE on pr_maj_dateFinTache TO L3_46_chefDeProjet;

--employe
GRANT SELECT on Competences TO L3_46_employe;
GRANT SELECT on Employe TO L3_46_employe;
GRANT SELECT on Projet TO L3_46_employe;
GRANT SELECT on Tache TO L3_46_employe;
GRANT SELECT on TacheProjet TO L3_46_employe;
GRANT SELECT on AssignationEmploye TO L3_46_employe;
