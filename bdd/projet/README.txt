Projet de bases de données
==========================

Lancer run_Projet.sql pour éxecuter toute la création des tables, triggers, procédures, etc. Ce script remplit également la base avec des valeurs appropriées pour effectuer des tests.

Pour vérifier le bon fonctionnement des triggers et fonctions PL/SQL, le fichier jeuxEssais contient des tests indépendants. Ce fichier n'entre pas en conflit avec les donénes du script principal et peut être lancé à tout moment.
