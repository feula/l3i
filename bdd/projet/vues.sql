--@vues.sql
DROP VIEW vue_moteurGraphiques;
DROP VIEW vue_jeux;
DROP VIEW vue_projetsEnCours;

CREATE VIEW vue_moteurGraphiques AS
SELECT *
FROM Projet
WHERE typeProjet = 'moteurGraphique';

CREATE VIEW vue_jeux AS
SELECT *
FROM Projet
WHERE typeProjet = 'jeu';

CREATE VIEW vue_projetsEnCours AS
SELECT *
FROM Projet
WHERE dateFin = NULL;