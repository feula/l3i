Bonjour,

Nous attendons dans vos projets :

- La création de vos tables avec :

¤ Index

¤ PK, FK

¤ Contraintes check

¤ Au moins un exemple de séquence

¤ Vos insertions

¤ Pour qu'on puisse le relancer plusieurs fois, prévoyez aussi les drop de tables/sequences

- L'utilisation du PSQL et des vues

¤ Trigger (2)

¤ Procédure/Fonction (2)

¤ Vues (2)

- La gestion des droits

¤ Création de rôles (2-3)

¤ Attribution de droits aux rôles

¤ Attribution de rôles aux utilisateurs (vos L3_XX)

¤ seront aussi attendus des gestions d'exception

Les chiffres entre parenthèses sont des chiffres objectifs.

Tout ce qui vous ai demandé sera vu en TD/TP/Cours.

Il est inutile de faire trop de triggers, procédures, etc. Nous préférons avoir un minimum, mais bien réalisé et pensé plutôt que trop.

Pour rappel, vous aurez à nous rendre :

- Un rapport au format PDF

- Un script principal qui contient votre projet (ou fait appel aux différents scripts composant votre projet)

- Des jeux d'essais que vous pourrez dérouler à votre soutenance (où vous testez vos contraintes, triggers, procédures, etc.)

Pensez également à vous référer à votre sujet de projet et n'hésitez pas à nous poser des questions.

Cordialement,

Isabelle Moizeau

-----------------------------------------------------------------------------------------------------------------------------------------

Bonjour à tous,

Dans un précédent mail je vous ai rappelé ce que nous attendions pour le projet.

Pour rappel, le rendu du projet se fera en semaine 10, vous aurez également votre soutenance à faire pour cette séance de TP là.

Petit rappels :

== DROITS
Nous attendons que vous définissiez des rôles cohérents avec votre projet.
Ex : pour un magasin :
- un administrateur (la personne qui a créé la base)
- un/des vendeurs
- un/des clients

Ces rôles sont ensuite à attribuer à vos comptes L3_XX.

A noter que la personne qui a créé tous les objets a d'office tous les droits dessus (d'où son rôle administateur par défaut).

Pour rappel, des exemples de syntaxe :
create role vendeur;
grant select on T to vendeur;
grant vendeur to L3_96;

/!\ Pensez bien à ajouter un "commit" après vos inserts dans vos scripts. Cela valide les données. Tant qu'elles ne le sont pas, vos collègues à qui vous avez donné les droits sur les tables ne pourront pas voir les données modifiées.

insert into T values(1);
insert into T values(2);
... 
commit;

/!\ Pour atteindre une table sur laquelle on vous a donné les droits il faut la préfixer par son owner.

Exemple :
L3_1 donne le droit select sur T à L3_2
Pour atteindre la table T de l'utilisateur L3_1, L3_2 sera obligé de faire :
select * from L3_1.T;
au lieu de select * from T;

Alternative :
L3_2 peut aussi créer un synonyme qui pointera vers la table de L3_1.
Exemple :
create synonym T for L3_1 T;

/!\ Pour que les droits que l'on vous a donné soient pris en compte il faut que vous quittiez sqlplus et que vous vous reconnectiez. Cela recréé une session qui elle aura les droits.

== VUES
Nous attendons également des vues.

Plusieurs angles d'attaque pour vous aider :
- Dans votre projet vous imaginez qu'une requête select pourrait être utilisée très régulièrement. (ex : dans une bibliothèque on aimerait savoir les livres empruntés par les femmes et les scripts empruntés par les hommes régulièrement)

- Un angle plus sécurité. Vous voulez donner les droits sur une vue plutôt qu'une table parce que votre vue contiendra moins de colonnes ou lignes (elle pourrait ne pas contenir la notion de salaire par exemple)

exemple de syntaxe :
create view MaVue as
select *
from Emprunts
where sexe = 'F';

== TRIGGERS/PROCEDURES/FONCTIONS

- Rappelez-vous de bien mettre "set serveroutput on" aux débuts de vos scripts pour que les sorties d'écrans générées par "dbms_output.put_line("xx")" soient pris en compte.

- Pensez à rajouter des "show error" après vos blocs et créations d'objets PL/SQL pour afficher des erreurs plus détaillées pour vous débloquer.

La seule chose qui vous restera à voir sera les index et vous les verrez dans le TP de la rentrée.

Bon courage

Cordialement,

Isabelle Moizeau