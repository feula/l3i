--@triggers.sql

set serveroutput on;

CREATE OR REPLACE TRIGGER tr_tacheProjet_insert 
BEFORE INSERT ON TacheProjet
FOR EACH ROW
DECLARE
	erreur_not_null EXCEPTION;
BEGIN
	IF :new.dateFinTache IS NOT NULL
	THEN raise erreur_not_null;
	END IF;
	EXCEPTION
	WHEN erreur_not_null THEN
		RAISE_APPLICATION_ERROR(-20001,'la date de fin doit être null à l''insertion');
END;
/

CREATE OR REPLACE TRIGGER tr_tacheProjet_update
BEFORE UPDATE ON TacheProjet
FOR EACH ROW
DECLARE
	erreur_dateFinTache EXCEPTION;
BEGIN
	IF :new.dateFinTache < :new.dateDebutTache
	THEN raise erreur_dateFinTache;
	END IF;
	EXCEPTION
		WHEN erreur_dateFinTache THEN
			RAISE_APPLICATION_ERROR(-20002,'la date de fin ne peut être antérieure à celle de départ');
END;
/

CREATE OR REPLACE TRIGGER tr_assignationEmpl_taches 
BEFORE INSERT ON AssignationEmploye
FOR EACH ROW
DECLARE
	nbTache NUMBER;
	erreur_nbTache EXCEPTION;
BEGIN
	SELECT COUNT(nomTache) INTO nbTache 
	FROM AssignationEmploye 
	GROUP BY idEmpl 
	HAVING idEmpl = :new.idEmpl;
	IF nbTache >= 5 THEN 
		raise erreur_nbTache;
	END IF;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		-- Si on ne trouve pas de données, on peut procéder à l'insertion.
		NULL;
	WHEN erreur_nbTache THEN
		RAISE_APPLICATION_ERROR(-20003,'Cet employé est déjà assigné au nombre maximum de taches');
END;
/

CREATE OR REPLACE TRIGGER tr_assignationEmpl_competences
BEFORE INSERT ON AssignationEmploye
FOR EACH ROW
DECLARE
	competencePossedee NUMBER;
	competenceNecessaire NUMBER;
	erreur_competence EXCEPTION;
BEGIN
	SELECT idCompetenceEmpl into competencePossedee FROM Employe WHERE idEmpl = :new.idEmpl;
	SELECT idCompetenceTache into competenceNecessaire FROM Tache WHERE  nomTache = :new.nomTache;
	IF competencePossedee != competenceNecessaire
	THEN raise erreur_competence;
	END IF;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		-- Si on ne trouve pas de données, on peut procéder à l'insertion.
		NULL;
	WHEN erreur_competence THEN
			RAISE_APPLICATION_ERROR(-20004,'Cet employé ne possède pas la compétence requise pour cette tâche');
END;
/

CREATE OR REPLACE TRIGGER tr_Projet_dateFin_nbVentes
BEFORE INSERT ON Projet
FOR EACH ROW
DECLARE
	erreur_not_null EXCEPTION;
BEGIN
	IF :new.dateFin IS NOT NULL or :new.nbventes IS NOT NULL
	THEN raise erreur_not_null;
	END IF;
	EXCEPTION
	WHEN erreur_not_null THEN
		RAISE_APPLICATION_ERROR(-20005,'la date de fin et le nombre de ventes doit être null à l''insertion');
END;
/

CREATE OR REPLACE TRIGGER tr_Projet_moteurGraphique
BEFORE INSERT ON Projet
FOR EACH ROW
DECLARE
	nbMoteurGraphique NUMBER;
	erreur_moteurGraphique EXCEPTION;
BEGIN
	SELECT COUNT(*) into nbMoteurGraphique FROM vue_moteurGraphiques WHERE nomProjet = :new.moteurGraphique and dateFin IS NOT NULL;
	IF nbMoteurGraphique = 0 and :new.moteurGraphique IS NOT NULL
	THEN raise erreur_moteurGraphique;
	END IF;
	EXCEPTION
		WHEN erreur_moteurGraphique THEN
			RAISE_APPLICATION_ERROR(-20006,'Ce moteur graphique n''est pas fini ou n''existe pas');
END;
/

CREATE OR REPLACE TRIGGER tr_Salaires_salaire
BEFORE INSERT OR UPDATE ON Salaires
FOR EACH ROW
DECLARE
	PRAGMA autonomous_transaction;
	CURSOR C1 IS SELECT * FROM Salaires WHERE idCompetenceEmpl = :new.idCompetenceEmpl;
	erreur_salaire_bas EXCEPTION;
	erreur_salaire_haut EXCEPTION;
BEGIN
	FOR ligne in C1 LOOP
		IF ligne.niveauCompetence < :new.niveauCompetence
		THEN 
			IF ligne.salaire>:new.salaire
			THEN raise erreur_salaire_bas;
			END IF;
		ELSE 
			IF ligne.niveauCompetence > :new.niveauCompetence THEN
				IF ligne.salaire<:new.salaire THEN 
					raise erreur_salaire_haut;
				END IF;
			ELSE
				NULL;
			END IF;
		END IF;
	END LOOP;
	EXCEPTION
		WHEN erreur_salaire_bas THEN
			RAISE_APPLICATION_ERROR(-20007,'Le salaire rentré est trop bas comparé au niveau de compétence');
		WHEN erreur_salaire_haut THEN
			RAISE_APPLICATION_ERROR(-20008,'Le salaire rentré est trop haut comparé au niveau de compétence');
END;
/
