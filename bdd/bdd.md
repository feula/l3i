# Bases de données

## CM1

__Degré d'une relation__: Nombre d'attributs. $\sigma(h)$
__Cardinalité d'une relation__: Nombre d'instances d'une table. $|h|$

### Algèbre relationnel: Opérateurs

__Opérateurs unaires__

* SELECTION: $\sigma$
  Equivalent SQL: `WHERE`
* PROJECTION: $\Pi$
  Equivalent SQL: `SELECT`


__Exemple de combinaison__:

$T = \sigma_{codemat=10}(Notes)$ 	┐
​                      	├── $T = \Pi_{cc,exam}(\sigma_{codemat=10}(Notes))$
$T = \Pi_{cc, exam}(Notes)$   	┘

__Opérateurs de base ensembliste__:

* JOINTURE: $\Join$
  $T = \pi_{nom, cc, note}(Etudiant {\Join_{\substack{e.codemat = n.codemat \\e.etu = n.etu}}} Notes)$

  Référence: [A visual explanation of SQL Joins (blog.codinghorror.com)](https://blog.codinghorror.com/a-visual-explanation-of-sql-joins/)

  ![joins](./joins.jpg)

__Exemples__:

$ (Matieres \Join_{m.codemat = n.codemat} Notes) $

# TD1

__Q1__

```plsql
CREATE TABLE Employe (
  -- Fields
  	matrempl char(2),
         nom varchar2(25),
      prenom varchar2(25),
     adresse varchar2(50),
      qualif varchar2(25),
  servaffect char(2),
  -- Constraints
  CONSTRAINT pk_employe PRIMARY KEY (matrempl)
);

CREATE TABLE Service (
  -- Fields
   nuserv char(2),
  libserv varchar2(25),
  matrdir char(2),
  -- Constraints
  CONSTRAINT pk_service PRIMARY KEY (nuserv)
);

CREATE TABLE Projet (
  -- Fields
     nuproj char(3),
    libproj varchar2(25),
   matrchef char(2),
  datedebut date,
  -- Constraints
  CONSTRAINT pk_projet PRIMARY KEY (nuproj),
  CONSTRAINT fk_chef FOREIGN KEY matrchef REFERENCES Employe(matrempl)
);

CREATE TABLE Compoproj (
  nuproj char(3),
  nulot char(3),
  tpstotprevh number(4),
  tpsencoursh number(4),
  tpsencoursm number(2),
  
  CONSTRAINT pk_compoproj PRIMARY KEY (nuproj, nulot),
  CONSTRAINT fk_nuproj FOREIGN KEY nuproj REFERENCES Projet(nuproj)
);

CREATE TABLE Travail (
  nuproj char(3),
  nulot char(3),
  datejour date,
  matrempl char(2),
  tpsjoursm number(3),
  
  CONSTRAINT pk_travail PRIMARY KEY (nuproj, nulot, datejour, matrempl),
  CONSTRAINT fk_nuproj FOREIGN KEY nuproj REFERENCES Projet(nuproj)
);
```

__Q2__:

1. Liste des employés comportant pour chacun le matricule, le nom, le prénom et le service d'affectation
   `SELECT matrempl, nom, prenom, servaffect FROM Employe;`
   $T = \Pi_{matrempl, nom, prenom, servaffect}(Employe)$

2. Liste des projets ayant démarré entre le premier janvier et le premier juillet 2012
   `SELECT * FROM Projet WHERE datedebut >= '01-JAN-2012' AND datedebut < '01-JUL-2012'`
   ou bien
   `SELECT * FROM Projet WHERE datedebut BETWEEN('01-JAN-2012', '01-JUL-2012')`
   $T = \Pi_*(\sigma_{01-JAN-2012 < datedebut < 01-JUL-2012}(Projet))$

3. Numéro des projets et de lots sur lesquels à travaillé l'employé de matricule 06PR14.

   ```plsql
   SELECT nuproj, nulot FROM Travail
   WHERE matrempl = '06PR14'
   ```

   $\Pi_{nuproj, nulot}(\sigma_{matrempl='06PR14'}(Travail))$

   Avec le libellé du projet:

   ```plsql
   SELECT nuproj, nulot, libproj, datedebut FROM Travail, Projet
   WHERE matrempl = '06PR14'
   AND Travail.nuproj = Projet.nuproj
   ```

   $\Pi_{\substack{nuproj \\ nulot \\ libproj \\ datedebut}}(\sigma_{\substack{matrempl = '06PR14' \\ Travail.nuproj = Projet.nuproj}}(Travail, Projet))$
   Avec le nom et prénom du chef:

   ```plsql
   SELECT nom, prenom, nuproj, nulot, libproj, datedebut
   FROM (Travail NATURAL JOIN Projet) JOIN Employe e
   ON matrchef = matrempl
   WHERE matrempl = '06PR14'

   -----------------------
   -- SQL1
   SELECT ...
   FROM Travail, Projet, Employe
   WHERE Travail.nuproj = Projet.nuproj AND
         Employe.matrempl = matrchef AND
         Travail.matrempl = '06PR14';

   ```

   $\Pi_{\substack{nom \\ prenom \\ nuproj \\ nulot \\ libproj \\ datedebut}}(\sigma_{matrempl='06PR14'}(Travail \Join Projet)) \Join_{matrchef = matrempl}(Employe)$

4. Numéros de projet et de lot sur lesquels l'employé de matricule 06PR14 à travaillé aujourd'hui avec pour chaque lot le temps qui lui a été globalement consacré jusque là.

   ```plsql
   SELECT nuproj, nulot FROM Travail
   WHERE datejour = TODAY()
   ```

__Q3__:

 1. ```plsql
      SELECT * FROM Employe, Travail
      WHERE Employe.matrempl = Travail.matrempl AND
            Travail.datejour = '01-MAR-2012' AND
            Travail.nuproj IN ('222', '333')
      ```










