# Bases de données - TP3
Florian EULA

##### Question 1
```sql
ALTER TABLE livres ADD note_moy number(4,2);
```

__Mise à jour d'un livre__
```sql
DECLARE
    book_id Livres.refl%TYPE;
BEGIN
    book_id := &pl;
    UPDATE Livres SET note_moy = (
        SELECT AVG(note) as note_moy FROM Avis WHERE refl = book_id 
        ) 
        WHERE refl = book_id;
END;
/
```

__Mise à jour de tous les livres__
```sql
DECLARE
    CURSOR lst IS (
        SELECT refl FROM Livres
    );
BEGIN
    FOR rec IN lst LOOP
        UPDATE Livres SET note_moy = (
            SELECT AVG(note) as note_moy FROM Avis WHERE Avis.refl = rec.refl 
            ) 
            WHERE Livres.refl = rec.refl;
    END LOOP;
END;
/
```

__Création du trigger__
```sql
CREATE OR REPLACE TRIGGER update_note_moy
AFTER INSERT
ON Avis
FOR EACH ROW
DECLARE
    PRAGMA autonomous_transaction;
BEGIN
    UPDATE Livres SET note_moy = (
        SELECT AVG(note) as note_moy FROM Avis WHERE Avis.refl = :NEW.refl 
    ) 
    WHERE Livres.refl = :NEW.refl;
    COMMIT;
END;
/
```
```sql
/*CREATE TABLE parcours(
    idp varchar2(10),
    intitulep varchar2(15),
    genre varchar2(15),
    date_deb date,
    CONSTRAINT pk_parcours PRIMARY KEY (idp)
);

CREATE TABLE compo_parcours(
    idp varchar2(10),
    id_evt varchar2(10),
    CONSTRAINT fk_compo_parcours_idp FOREIGN KEY (idp) REFERENCES parcours(idp)
);

CREATE TABLE inscrip_parcours(
    idcl number,
    idp varchar2(10),

    CONSTRAINT fk_inscrip_parcours_idcl FOREIGN KEY (idcl) REFERENCES client(idcl),
    CONSTRAINT fk_inscrip_parcours_idp FOREIGN KEY (idp) REFERENCES parcours(idp)
);

CREATE TABLE inscrip_evt(
    idcl number,
    idp varchar2(10),
    id_evt varchar2(10),

    CONSTRAINT fk_inscrip_evt_idcl FOREIGN KEY (idcl) REFERENCES client(idcl),
    CONSTRAINT fk_inscrip_evt_idp FOREIGN KEY (idp) REFERENCES parcours(idp)
);*/
```