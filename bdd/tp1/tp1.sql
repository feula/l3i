-- # Base de données - TP1 - Rappels SQL
-- Florian Eula

-- ### Q1 - Création du schema & données
-- Dropping tables
DROP TABLE Clients CASCADE CONSTRAINTS;
DROP TABLE Livres CASCADE CONSTRAINTS;
DROP TABLE Achats CASCADE CONSTRAINTS;
DROP TABLE Avis CASCADE CONSTRAINTS;

-- Table Clients
-- idcl clé primaire
CREATE TABLE Clients (
    -- Attributs
    idcl number,
     nom varchar2(20),
    pren varchar2(15),
     adr varchar2(30),
     tel varchar2(12),
     -- Contraintes
    CONSTRAINT pk_clients PRIMARY KEY (idcl)
);

-- Table Livres
-- refl clé primaire
CREATE TABLE Livres (
    -- Attributs
      refl varchar2(10),
     titre varchar2(20),
    auteur varchar2(20),
     genre varchar2(15),
    -- Contraintes
    CONSTRAINT pk_livres PRIMARY KEY (refl)
);

-- Table Achats
-- (idcl, refl, dateachat) clé primaire
-- idcl, refl clés étrangères
CREATE TABLE Achats (
    -- Attributs
         idcl number,
         refl varchar2(10),
    dateachat date,
    -- Contraintes
    CONSTRAINT fk_idcl_achats FOREIGN KEY (idcl) REFERENCES Clients(idcl),
    CONSTRAINT fk_refl_achats FOREIGN KEY (refl) REFERENCES Livres(refl),
    CONSTRAINT pk_achats PRIMARY KEY (idcl, refl, dateachat)
);

-- Table Avis
-- (idcl, refl) clé primaire
-- idcl, refl clés étrangères
CREATE TABLE Avis (
    -- Attributs
    idcl number,
    refl varchar2(10),
    note number(4,2),
    commentaire varchar2(50),
    -- Contraintes
    CONSTRAINT fk_idcl_avis FOREIGN KEY (idcl) REFERENCES Clients(idcl),
    CONSTRAINT fk_refl_avis FOREIGN KEY (refl) REFERENCES Livres(refl),
    CONSTRAINT pk_avis PRIMARY KEY (idcl, refl)
);

-- Insertion de tuples qui fonctionnent 
INSERT INTO Clients(idcl, nom, pren, adr, tel) VALUES (0, 'McBobbington', 'Bob', 'Jtenposedesquestions', '+33606060606'); 
INSERT INTO Clients(idcl, nom, pren, adr, tel) VALUES (1, 'José', 'José', 'Tes bien curieux', '0102030405');
INSERT INTO Clients(idcl, nom, pren, adr, tel) VALUES (2, 'Eula', 'Florian', 'bah non en fait.', '0203040506');
 
INSERT INTO Livres(refl, titre, auteur, genre) VALUES ('TTLSDLL', 'Secret de la Licorne', 'Hergé', 'Science-Fiction');
INSERT INTO Livres(refl, titre, auteur, genre) VALUES ('TTCES', 'Coke en Stock', 'Hergé', 'Histoire vraie');
INSERT INTO Livres(refl, titre, auteur, genre) VALUES ('TTTTAC', 'Tintin au Congo', 'Hergé', 'Colonialisme');

INSERT INTO Achats(idcl, refl, dateachat) VALUES (0, 'TTCES', TO_DATE('13/01/2017', 'dd/MM/yyyy'));
INSERT INTO Achats(idcl, refl, dateachat) VALUES (0, 'TTLSDLL', TO_DATE('12/01/2017', 'dd/MM/yyyy'));
INSERT INTO Achats(idcl, refl, dateachat) VALUES (1, 'TTTTAC', TO_DATE('03/01/1970', 'dd/MM/yyyy'));
INSERT INTO Achats(idcl, refl, dateachat) VALUES (2, 'TTTTAC', TO_DATE('03/01/1970', 'dd/MM/yyyy'));
 
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (0, 'TTCES', 4.3, 'Le livre de lannée.');
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (2, 'TTLSDLL', 11, 'Bof');
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (1, 'TTTTAC', 18.0, 'Dun frappant réalisme.');
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (2, 'TTTTAC', 15.0, 'zzzzz');
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (2, 'TTCES', 20.0, '');
INSERT INTO Avis(idcl, refl, note, commentaire) VALUES (1, 'TTCES', 20.0, null);

-- Insertion de tuples violant les Contraintes
-- Ne fonctionne pas volontairement, s'attendre à des erreurs dans le terminal
-- `Clients.idcl` déjà utilisé
INSERT INTO Clients(idcl, nom, pren, adr, tel) VALUES (0, 'No', 'No', 'No', 'No');
-- `Livres.refl` déjà utilisé
INSERT INTO Livres(refl, titre, auteur, genre) VALUES ('TTCES', 'No', 'No', 'No');
-- `Achats`: FK non respectée
INSERT INTO Achats(idcl, refl, dateachat) VALUES (9, 'TTBEG', TO_DATE('15/02/1990', 'dd/MM/yyyy'));
-- `Achats`: PK non respectée (NULL)
INSERT INTO Achats(idcl, refl, dateachat) VALUES (null, null, null);

-- ### Q2 - Requètes SQL
-- #### Meilleures ventes
-- Remplacer COUNT(refl) > 10000 par COUNT(refl) > 1 pour tester sur le jeu de données;
SELECT titre, auteur, genre FROM Livres
INNER JOIN (
    SELECT refl FROM ACHATS
        GROUP BY refl
        HAVING COUNT(refl) > 10000
) achats_filtered ON Livres.refl = achats_filtered.refl;

-- #### Note moyenne supérieure à 16 
SELECT titre, auteur, genre FROM Livres
INNER JOIN (
    SELECT refl FROM Avis
        HAVING AVG(note) > 16.0
        GROUP BY refl, note
) avis_filtered ON Livres.refl = avis_filtered.refl;

-- #### Clients n'ayant pas renseigné l'attribut commentaire
SELECT nom, pren, titre, note FROM Avis
LEFT JOIN Livres ON (Avis.refl = Livres.refl)
LEFT JOIN Clients on (Avis.idcl = Clients.idcl)
WHERE Avis.commentaire = '' OR Avis.commentaire IS NULL;


