# Programmation fonctionnelle

## Types

|               |          |                     |
| ------------- | -------- | ------------------- |
| $n \geq 0 = $ | $e + e'$ | $ n + \frac{e}{e'}$ |

```ocaml
type expr =   C of int 
            | Plus of expr * expr 
            | PlusDiv of int * expr * expr
(* Correspond aux trois représentations possibles visibles dans le tableau au dessus*)

(* Expressions à tester:
   - 1789                   => 1 ligne, 4 colonnes
   - 13 + 5                 => 1 ligne, 6 colonnes
   
         13
   - 1 + --                 => 3 lignes, 6 colonnes
          7
          
          3 
   - 5 +  - + 4            => 3 lignes, 9 colonnes
          4
                         9
                     5 + -
           6             2
   - 1 + ----- + 2 + ----- => 7 lignes, 21 colonnes
             8        1317
         1 + -
             5          
*)
```

__1__. Modéliser l'expression (4) par expr

```ocaml
Plus(PlusDiv(4, C(3), C(4)), C(5))
```

__2__. Ecrire une fonction `nb_colonnes: expr -> int`

```ocaml
let rec nb_colonnes exp = match exp with
	  C(x) -> nb_chiffres(x)
	| Plus(x, y) -> nb_colonnes(x) + 3 + nb_colonnes(y) (* 3 colonnes pour le ' + ' *)
	| PlusDiv(x, y, z) -> nb_chiffres(x) + 3 + max (nb_colonnes(y)) (nb_colonnes(z));;
```

__3__. Ecrire une fonction `nb_lignes: expr -> int`

```ocaml
let rec nb_lignes exp = match exp with
	  C(x) -> 1
	| Plus(x, y) -> nb_lignes(x) + nb_lignes(y)
	| PlusDiv(x, y, z) -> 1 + nb_lignes(y) + nb_lignes(z);;


let rec nb_lignes exp = match exp with
	  C(x) -> 1
	| Plus(x, y) -> (match x,y with
		  (C(a), C(b)) -> 0
		| (_, _) -> nb_lignes(x) + nb_lignes(y))
	| PlusDiv(x, y, z) -> (match y,z with
		  (C(a), C(b)) -> 2
		| (a, b) -> 1 + nb_lignes(a) + nb_lignes(b));;

nb_lignes(Plus(PlusDiv(1, C 6, PlusDiv(1, C 8, C 5)), PlusDiv(2, PlusDiv(5, C 9, C 2), C(1))));;
```

On suppose qu'on a une fonction `nb_chiffres: int -> int`