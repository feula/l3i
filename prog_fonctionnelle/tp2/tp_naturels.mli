(* ***************************************************************** *)
(* ***************************************************************** *)

(* conversion (le parametre doit etre >= 0) *)

val naturel_of_int : int -> naturel

(* ----------------------------------------------------------------- *)

(* conversion inverse *)

val int_of_naturel : naturel -> int

(* ***************************************************************** *)

(* x -> true si x est pair, false sinon *)

val est_pair : naturel -> bool

(* ***************************************************************** *)

(* (x , y) -> x + y *)

val plus : naturel * naturel -> naturel

(* ----------------------------------------------------------------- *)

(* (x , y) -> x * y *)

(* utiliser plus *)

val fois : naturel * naturel -> naturel

(* ----------------------------------------------------------------- *)

(* (x , y) -> true si x >= y, false sinon *)

val supegal : naturel * naturel -> bool

(* ----------------------------------------------------------------- *)

(* (x , y) -> x - y (avec x >= y) *)

val moins : naturel * naturel -> naturel

(* ***************************************************************** *)

(* (a , b) -> (q , r) (avec b > 0 et a = b * q + r et 0 <= r < q *)

(* utiliser supegal, moins et plus (ou simplement suc si disponible) *)

(* ne pas definir de fonction auxiliaire, en particulier des fonctions
   calculant separement un quotient et un reste
*)

val quotient_et_reste : naturel * naturel -> naturel * naturel

(* ***************************************************************** *)

(* x -> true si x est une puissance de deux, false sinon *)

val est_puissance_de_deux : naturel -> bool

(* ----------------------------------------------------------------- *)

(* x -> le logarithme entier en base deux de x (avec x > 0) *)

val log_base_deux : naturel -> naturel

(* ***************************************************************** *)
(* ***************************************************************** *)