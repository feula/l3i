# Sujet TP
__Nat1__: naturels construits à partir de zéro et de successeurs
__Nat2__: naturels construits à partir de zéro et de un, construit à partir de (+2; -2)
__Nat3__: vision arborescente. Base: zéro: d0 non applicable à zéro.
0 -> d1 -> 1 -> d0 -> 2 -> d0 -> 4
                        -> d1 -> 5
             -> d1 -> 3 -> d0 -> 6
                        -> d1 -> 7
                              ...

## Compilation
.mli -> interface, définit les membres publics
.ml  -> source

Compilation d'un .mli donne un .cli
Compilation d'un .ml donne un .cmo avec ocamlc et un .cmx avec ocamlopt

## Chargement
`#load "<module.cmo>";;` pour le rendre disponible dans l'interpreteur.
Une fois chargé, utilisable via Nat_1.<fun>

`open module;;` pour le rendre utilisable et charger toutes les fonctions dans le namespace global
Une fois chargé, utilisable via <fun>

`suc(suc(zero()))` afficherait `- : Nat_1.naturel = <abstr>`
Pour pouvoir afficher ce type, on utilise `#install_printer <printer_func>`