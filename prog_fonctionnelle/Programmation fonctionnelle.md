# Programmation fonctionnelle

## Inférence de types

`function x -> x` est générique. Dans la sortie ` - : 'a -> 'a = <fun>`, `'a` indique qu'on ne connait pas le type, et peut être n'importe quoi. Ces types sont notés `'` suivi d'une lettre.

`let a = 3 + 2 in 2 * 3` est une expression locale.
Une expression est locale si `let` est suivi par `in`, et globale sinon.
`a` est lié à 5, uniquement dans le `in`

`let a = 7` est une définition liaison globale.
`val a : int = 8` 

L'opérateur de comparaison `=` est polymorphe. Sa définition est `'a -> 'a -> bool`. En comparaison, l'opérateur d'addition `+` est spécifique aux entiers (`int -> int -> int = <fun>`)

### Ex 1

Toutes les expressions OCaml suivantes sont syntaxiquement correctes. Pour chacune, dire si
elle a une valeur. Dans ce cas : laquelle et de quel type ? Dans le cas contraire : pourquoi ?

```ocaml
5 * 4 * 3 * 2 * 1 ;;
```
Possède une valeur, 120, de type `int`

```ocaml
true ;;
```
Possède une valeur, true, de type `bool`.

```ocaml
if 2 > 0 then "true" else "false" ;;
```
Possède une valeur, "true", de type `string`.

```ocaml
if 2 > 0 then true else false ;;
```
Possède une valeur, true, de type `bool`.

```ocaml
if 2 > 0 then "oui" else "non" ;;
```
Possède une valeur, "oui", de type `string`.

```ocaml
if 2 > 0 then oui else non ;;
```
Erreur. `oui` n'a jamais été bind.

```ocaml
if 2 > 0 then true else "false" ;;
```
Erreur. "false" est un `string`, mais le type attendu était `bool` (car true est un `bool`)

```ocaml
3.14 +. 2. ;;
```
Possède une valeur, 5.140000000000057 (lolfloat), de type `float`.

```ocaml
3.14 +. 2 ;;
```
Erreur. 3.14 est un `float`, 2 est un `int`, +. ne marche qu'avec deux `float`.

```ocaml
3.14 + 2.0 ;;
```
Erreur. 3.14 est un `float`, 2.0 est un `float`, + ne fonctionne qu'avec deux `int`.

```ocaml
3.14 > 2. ;;
```
Possède une valeur, true, de type `bool`

```ocaml
3.14 > 2 ;;
```
Erreur. Aucun opérateur > prenant un `float` et un `int`.

```ocaml
int_of_float (3.14) > 2 ;;
```
Possède une valeur, true, de type `bool`

```ocaml
3.14 > float_of_int (2) ;;
```
Possède une valeur, true, de type `bool`

```ocaml
3 > 2 ;;
```
Possède une valeur, true, de type `bool`

```ocaml
"trois" > "deux" ;;
```
Possède une valeur, true, de type `bool`.

```ocaml
"deux" > "un" ;;
```
Possède une valeur, false, de type `bool`

```ocaml
5 / 2 ;;
```
Possède une valeur, 2, de type `int`

```ocaml
5. /. 2. ;;
```
Possède une valeur, 2.5, de type `float`

```ocaml
sqrt (4.) ;;
```
Possède une valeur, 2., de type `float`

```ocaml
sqrt (4) ;;
```
Erreur. sqrt() attend un `float`

```ocaml
sqrt ;;
```
Possède une valeur <fun>, de type `float -> float`

```ocaml
exp (1.) ;;
```
Possède une valeur e, de type `float`

```ocaml
log (exp (1.)) ;;
```
Possède une valeur 1., de type `float`

```ocaml
5 / 0 ;;
```
Exception, division par zéro.

```ocaml
if true then 5 / 0 else 3 ;;
```
Exception, division par zéro.

```ocaml
if true then 3 else 5 / 0 ;;
```
Possède une valeur 3 de type `int`

```ocaml
if false then 5 / 0 else 3 ;;
```
Possède une valeur 3 de type `int`

```ocaml
if false then 3 else 5 / 0 ;;
```
Exception, division par zéro.

```ocaml
(2 / 0 = 5) && true ;;
```
Exception, division par zéro.

```ocaml
true && (2 / 0 = 5) ;;
```
Exception, division par zéro. Le true avant ne bloque pas l'évaluation du reste de l'expression

```ocaml
(2 / 0 = 5) && false ;;
```
Exception, division par zéro.

```ocaml
false && (2 / 0 = 5) ;;
```
Possède une valeur false, de type `bool`. false court-circuite le reste de l'expression.

```ocaml
(2 / 0 = 5) || true ;;
```
Division par zéro, évaluée en premier.

```ocaml
true || (2 / 0 = 5) ;;
```
Possède une valeur true, de type `bool`. Ici, true court-circuite le reste de l'expression

```ocaml
(2 / 0 = 5) || false ;;
```
Division par zéro, évaluée en premier.

```ocaml
false || (2 / 0 = 5) ;;
```
Division par zéro, évaluée dnas tous les cas, false ne court-circuite pas un ||

```ocaml
"s" ^ "i" ;;
```
Possède une valeur "si", de type `chaine`.

```ocaml
's' ^ "i" ;;
```
Erreur. L'opérateur de concaténation attend deux `string`

```ocaml
's' ^ 'i' ;;
```
Erreur. L'opérateur de concaténation attend deux `string`

```ocaml
"s" ^ i ;;
```
Erreur. i n'est pas bind.

```ocaml
si ;;
```
Erreur. si n'est pas bind.

```ocaml
"if" ;;
```
Possède une valeur "if", de type `string`

```ocaml
function x -> x > 0 ;;
```
Possède une valeur <fun>, de type `'a -> bool`

```ocaml
(function x -> x > 0) (12) ;;
```
Possède une valeur true, de type `bool`

```ocaml
function x -> 2 * x ;;
```
Possède une valeur <fun>, de type `int -> int`

```ocaml
(function x -> 2 * x) (7) ;;
```
Possède une valeur 14, de type `int`

```ocaml
function (x, y) -> (x + y, x - y) ;;
```
Possède une valeur <fun>, de type `int * int -> int * int`

```ocaml
function x -> x / 0 ;;
```
Possède une valeur <fun>, de type `int -> int`

```ocaml
(function x -> x / 0) (12) ;;
```
Exception, division par zéro lorsque la fonction est évaluée.

```ocaml
(function x -> x) (5) ;;
```
Possède une valeur 5 de type `int`

```ocaml
(function x -> x) ("oui") ;;
```
Possède une valeur "oui" de type `string`

```ocaml
function x -> x ;;
```
Possède une valeur <fun> de type `'a -> 'a`

```ocaml
(function x -> 1) (5) ;;
```
Possède une valeur 1 de type `int`

```ocaml
(function x -> 1) (5.) ;;
```
Possède une valeur 1, de type `int`

```ocaml
function x -> 1 ;;
```
Possède une valeur <fun> de type `'a -> int`

```ocaml
(function (x, y) -> (y, x)) (1, 2) ;;
```
Possède une valeur (2, 1) de type `int * int`

```ocaml
(function (x, y) -> (y, x)) (1, "oui") ;;
```
Possède une valeur ("oui", 1) de type `string * int`

```ocaml
(function (x, y) -> (y, x)) ("oui", "oui") ;;
```
Possède une valeur ("oui", "oui") de type `string * string`

```ocaml
function (x, y) -> (y, x) ;;
```
Possède une valeur <fun> de type `'a * 'b -> 'b * 'a`

```ocaml
function x -> (x, x) ;;
```
Possède une valeur <fun>, de type `'a -> 'a * 'a`

```ocaml
(function x -> (x, x)) (1) ;;
```
Possède une valeur (1,1) de type `int * int`

```ocaml
let aaa = 2 and bbb = 3 * aaa in aaa + bbb ;;
```
Erreur. aaa n'est pas encore bind lorsqu'on l'utilise dans bbb

```ocaml
let aaa = 2 in let bbb = 3 * aaa in aaa + bbb ;;
```
Possède une valeur 8 de type `int`

```ocaml
let aaa = 2 in let bbb = 3 * aAa in aaa + bbb ;;
```

Erreur. aAa n'est pas bind.

### Ex 2

Toutes les phrases OCaml suivantes sont syntaxiquement correctes. On suppose qu’elles sont interprétées l’une après l’autre. Pour chacune, dire si c’est une définition globale let . . . = e ou une expression e . Dire si e a une valeur. Dans ce cas : laquelle et de quel type ? Dans le cas contraire : pourquoi ?

```ocaml
let absolu = function x -> if x >= 0 then x else - x ;;
```
Définition globale. Valeur de e: <fun> de type `int`

```ocaml
absolu (absolu (-12)) ;;
```
Expression. Valeur de e: 12 de type `int`

```ocaml
let comp = function (x, y, z) -> (x <= y, y <= z) ;;
```
Définition globale. Valeur de e: <fun> de type `'a * 'a * 'a -> bool * bool`

```ocaml
(comp (3, 2, 4), comp (2, 3, 4)) ;;
```
Expression. Valeur de e: ((false, true), (true, true)) de type `(bool * bool) * (bool * bool)`

```ocaml
comp ("jaune", "rouge", "vert") ;;
```
Expression. Valeur de e: (false, true) de type `bool * bool`

```ocaml
let approx = function (a, b) -> let (b1, b2) = comp (a - 1, b, a + 1) in b1 && b2 ;;
```
Définition globale. Valeur de e: <fun> de type `int * int -> bool`

```ocaml
approx (12, 13) ;;
```
Expression. Valeur de e: true de type `bool`

```ocaml
approx ("mauve", "violet") ;;
```
Erreur. Difficile de soustraire/ajouter un entier à une chaine.

```ocaml
let ff = function x -> x + 1 in ff ;;
```
Déclaration locale. Valeur e: <fun> de type `int -> int`

```ocaml
ff (5) ;;
```
Erreur. ff déclarée localement

```ocaml
let ff = function x -> x + 1 ;;
```
Déclaration globale. e: <fun> `int -> int`

```ocaml
ff (5) ;;
```
Valeur e = 6, `int`

```ocaml
let ff = function x -> x + 1. ;;
```
Erreur de type (`int` plus_int `float`

```ocaml
ff (5) ;;
```
Erreur.

```ocaml
let ff = function x -> x +. 1. ;;
```
Déclaration globale. e :<fun> `float -> float`

```ocaml
ff (5) ;;
```
e = 6.0, `float`

```ocaml
(let ff = function x -> x + 1 in ff) (5) ;;
```
Valeur e = 6, `int`

```ocaml
ff (5) ;;
```
e = 6.0, `int`

```ocaml
ff ;;
```
Valeur <fun> type `int -> int`

```ocaml
let fff = function b -> b ;;
```
Déclaration globale, valeur <fun>, type `'a -> 'a`

```ocaml
fff (3) ;;
```
Valeur e = 3, type `int`

```ocaml
fff ("oui") ;;
```
Valeur e = "oui", type `string`

```ocaml
let ffff c = c ;;
```
Déclaration globale, valeur <fun>, type `'a -> 'a`

```ocaml
ffff (3) ;;
```
Valeur 3 `int`

```ocaml
ffff ("oui") ;;
```
Valeur "oui" `string`

### Ex 3

__int -> int__:
Valeur: function x -> x + 1
Evalué: int -> int = <fun>
Application: f(5)

__int * int -> bool__:
Valeur: function x y -> x = y
Evalué: 

__string -> bool__:
Valeur: function x -> x = "José"

__float * float -> string__:
Valeur: function x y -> if x +. 1. = y then "oui" else "non"

__bool * bool -> bool__:
Valeur: function x y -> x && y

__int -> int * bool__:
Valeur: function x -> (x + 1, true)

### Ex 4

```
let g = function x,y -> if x + 1 = 2 && y then 2 else 4;;
let f = function (x,y) -> (x,y);;
```

