# Recherche Opérationnelle - Rapport
Florian Eula

#### 1 - Compréhension et observation
`scp.mod` est la modélisation d'un problème d'optimisation de couts(?)
`scpe5.dat` est résolu rapidement, avec x[1], x[18], x[74], x[99] et x[330] prenant la valeur 1 et tous les autres x la valeur 0.
Les données présentes dans le fichier présentent une matrice de cette forme:
```
[ 4 8 10 11 12 ...
[ 1 2  4  5 13 ...
[ 1 3  4  6  8 ...
...
[ 2 4  7  9 10 ... 477 486 495 495 ]
```
`scpclr13.dat` à assassiné ma machine pendant plusieurs minutes, utilisant une quantité intéressante de mémoire et de temps processeur. On peut supposer que des matrices de 4095 lignes avec 715 éléments par ligne prennent un peu de temps à être calculées et optimisées.

#### 2 - Exercices
__1__:
```mathprog
# Declaration des donnees du probleme

	param m; # nombre de contraintes (lignes) du probleme
	param n; # nombre de variables (colonnes) du probleme

	set indRow := 1..m; # indices des contraintes
	set indCol := 1..n; # indices des variables

	set matrice dimen 2; # Matrice creuse des contraintes	

	param obj{indCol}; # Vecteur des coefficients de la fonction objectif 

# Declaration d'un tableau de variables binaires
	
	var x{indCol} binary; # x[1..9] E {0, 1}

# Fonction objectif

	maximize cout : sum{j in indCol} obj[j]*x[j];

# Contraintes

	s.t. Contrainte{i in indRow} : sum{(i,j) in matrice} x[j] <= 1; 

# Resolution (qui est ajoutee en fin de fichier si on ne le precise pas)

	solve;

# Affichage des resultats

	display : x;	# affichage "standard" des variables
   
data;
	param m := 11; # Indice de contraintes
	param n := 9; # Nombre de variables par contrainte
	param obj := 1 1
                 2 3
                 3 7
                 4 3
                 5 12
                 6 4
                 7 9
                 8 4
                 9 3; # Facteurs de la fonction objectif pour X[i]
    set matrice :=  (1,*) 1 5 # = x1 + x5
                    (2,*) 2 5 # = x2 + x5
                    (3,*) 3 5 # ...
                    (4,*) 3 4
                    (5,*) 2 7
                    (6,*) 5 7
                    (7,*) 5 4
                    (8,*) 6 7
                    (9,*) 6 8
                   (10,*) 8 4
                   (11,*) 5 9;
end;
```
__Sortie__:
```
x[1].val = 1
x[2].val = 0
x[3].val = 1
x[4].val = 0
x[5].val = 0
x[6].val = 0
x[7].val = 1
x[8].val = 1
x[9].val = 1
```

__2.7__:
Exprimer les contraintes sous forme d'une matrice pose un petit challenge. Mais est-ce qu'on en a vraiment besoin?
```
# Declaration des donnees du probleme

	param m; # nombre de contraintes (lignes) du probleme
	param n; # nombre de variables (colonnes) du probleme


	set indRow := 1..m; # indices des contraintes
	set indCol := 1..n; # indices des variables

	set livr dimen 2; # Matrice creuse des contraintes	

	param coutLivr{indCol,indCol}; # Cout des livraisons
    param coutEntr{indCol}; # Cout des entrepots
    param capacite{indCol}; # Capacité des entrepots
    param demande{indCol};  # Demande de chaque dépot

# Declaration d'un tableau de variables binaires
	
	var livr{indCol,indCol} binary; # livr[1..12] E {0, 1}
    var c{indCol} binary; # c[1..12] E {0,1}

# Fonction objectif

	minimize cout : sum{i in indCol}(sum{j in indCol}(livr[i,j] * coutLivr[i,j]) + c[i]*coutEntr[i])

# Contraintes

	s.t. ContrainteCapacite{i in indRow} : sum{(i,j) in matrice} livr[j] <= capacite[i]; 
    s.t. ContrainteLivraison{i in indRow} : sum{(i,j) in matrice} x[j] <= capacite[i]; 

# Resolution (qui est ajoutee en fin de fichier si on ne le precise pas)

	solve;

# Affichage des resultats

	display : x;	# affichage "standard" des variables
   
data;
	param m := 11; # Indice de contraintes
	param n := 9; # Nombre de variables par contrainte
	param obj := 1 1
                 2 3
                 3 7
                 4 3
                 5 12
                 6 4
                 7 9
                 8 4
                 9 3; # Facteurs de la fonction objectif pour X[i]
    set matrice :=  (1,*) 1 5 # = x1 + x5
                    (2,*) 2 5 # = x2 + x5
                    (3,*) 3 5 # ...
                    (4,*) 3 4
                    (5,*) 2 7
                    (6,*) 5 7
                    (7,*) 5 4
                    (8,*) 6 7
                    (9,*) 6 8
                   (10,*) 8 4
                   (11,*) 5 9;
end;


```