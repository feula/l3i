/* Exercice 1.1 */
# Variables
	param DAM1;
	param DAM2;
	param DBM1;
	param DBM2;
	param HD1;
	param HD2;
	param valC1;
	param valC2;
	var x1 >= 0; /* Nombre de casquettes de type 1 */
	var x2 >= 0; /* Nombre de casquettes de type 2 */
# Fonction objectif
	maximize profit : valC1*x1 + valC2*x2;
# Contraintes
	s.t. Vente1 : DAM1*x1 + DAM2*x2 <= HD1;
	s.t. Vente2 : DBM1*x1 + DBM2*x2 <= HD2;
# Résolution
	solve;
# Affichage
	display : x1,x2;
	display : 'z=', valC1*x1 + valC2*x2;
data;
	param DAM1 := 0.2;
	param DAM2 := 0.4;
	param DBM1 := 0.2;
	param DBM2 := 0.6;
	param HD1  := 400;
	param HD2  := 800;
	param valC1 := 12;
	param valC2 := 20;
end;

