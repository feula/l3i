/* Exercice 1.4.2 */
# Déclarations
	param tailleM; # Nombre de créneaux horaires
	set M := 1..tailleM; # Ensemble des indices des créneaux horaires

	param heures{M}; # Tableau des demandes en personnel a chaque créneau
	var x{M} >= 0 integer; # Nombre d'infirmières commençant au créneau i
# Objectif
	minimize cost : sum {i in M}(x[i]);
# Contraintes
	s.t. Heure{i in M} : x[i mod 12 + 1] + 
                         x[(i + 11) mod 12 + 1] + 
                         x[(i + 9) mod 12 + 1] + 
                         x[(i + 8) mod 12 + 1] >= heures[i];
# Résolution
	solve;
# Affichage
	display : x;
	display : 'Infirmières travaillant:', sum{i in M} x[i];
data;
	param tailleM := 12;
	param heures := 1  35
                       2  40
                       3  40
                       4  35
                       5  30
                       6  30
                       7  35
                       8  30
                       9  20
                       10 15
                       11 15
                       12 15;
end;


