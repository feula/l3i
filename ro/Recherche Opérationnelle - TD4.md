# Recherche Opérationnelle - TD4

1. __Quel est le nombre d'itinéraires possibles?__

2. __On souhaite déterminer le temps de trajet de P à C dans le pire des cas__

   a.

   b. __Poser ce problème sous la forme d'un programme linéaire. Que penser de sa résolution à la main ?__

   c. __Quel est le temps de ce trajet?__
   $$
   max\ z = \sum[i = 0...10]\sum[j = 0...10]({Dist_{i,j} * E_{i,j}})
   $$

   ```python
   # LOL LES PERFS
   # O(n²) temporel, O(n²) spatial
   # tranquille
   def visit_node(node, path, sum, distance_list):
       if not node.has_children():
           distance_list[path] = sum
           return
       
       for child in node.children():
           visit_node(child, path + node.name, sum + worst_dist(node, child), distance_list)
           
   def find_worst(tree):
       max_dist = 0
       worst_path = ""
       distance_list = dict()
       visit_node(tree, tree.name, 0, distance_list)
       for k,v in distance_list:
           if v > max_dist:
               worst_path = k
               max_dist = v
               
   ```

   ```
   chemin <- {P}
   X <- {P}
   temps(P) = 0
   TantQue X is not V:
   	Choisir x in V\X tel que Pred(x) in X
   	temps(x) = max(temps(y) + pire_cout(y), x), y in Pred(x)
   	chemin = chemin + {y}
   	X = X + {x}
   ```

3. __On constate que l'itinéraire actuellement utilisé correspond à celui que l'on choisirait si l'on adoptait l'attitude la plus optimiste.__

   a) Enoncer précisément le problème que l'on doit résoudre pour répondre à cette question

   On remplace $max\ z$ par $min\ z$

4. Attitude la plus prudente => Prendre le meilleur pire

5. Plus faible marge de fluctuation => Prendre celui ou la différence entre meilleur et pire est minimale

