route add -net default gw 192.168.1.254
activation d'ip forward

Règle de masquage d'ip:

```
a)
iptables -A FORWARD -i eth2 -d 192.168.1.0/24 -m conntrack --ctstate NEW -j REJECT
iptables -A FORWARD -i eth2 -d 10.0.0.0/8 -m conntrack --ctstate NEW -j REJECT

b)
iptables -A FORWARD -p tcp -i eth0 --dport 80 -j ACCEPT
iptables -A FORWARD -p tcp -i eth1 --dport 80 -j ACCEPT

c)
iptables -t nat -A POSTROUTING -o eth2 -j SNAT --to 145.12.0.53
```

3/
iptables -A FORWARD -p tcp -i eth2 --dport 22 -j ACCEPT
iptables -A FORWARD -p tcp -i eth2 --dport 80 -j ACCEPT

à ajouter avant les DROP
