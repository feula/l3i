6 machines, pinocchio, machine <-simple-> switch, switch <= croisé => switch
194.85.40.0/21 => Broadcast en 194.85.47.255
Pour ovtenir les deux sous réseaux: On veut diviser le nombre de machines par réseau par 2. Puisque le masque correspond au nombre de bits mis à 1 dans le masque, il suffit d'un flip un de plus pour diviser par deux le nombre d'adresses disponibles. En effectuant ipcalc 194.85.40.0/22 -bnmp, on obtient
NETMASK = 255.255.252.0
BROADCAST = 194.85.43.255
On connait donc la plage du premier sous réseau: [194.85.40.0 ; 194.85.43.255]
Par conséquent, la plage du deuxieme sous réseau sera [194.85.44.0; 194.85.47.255] (arrivant à la fin du /21). On peut vérifier ceci avec ipcalc 194.85.44.0/22: 
NETMASK = 255.255.252.0
BROADCAST = 194.85.47.255
Ce qui correspond bien a la fin de la plage "générale"
On va associer les machines aux ips correspondant à leur numéro pour des raisons pratiques (plus facle à reconnaitre), on aura donc:

m1 : réseau 1 : 194.85.40.1
m2 : réseau 2 : 194.85.44.2
m3 : réseau 1 : 194.85.40.3
m4 : réseau 2 : 194.85.44.4
m5 : réseau 2 : 194.85.44.5
m6 : réseau 1 : 194.85.40.6

# Alias de carte

m5 : lan3 : 10.10.10.5
m6 : lan3 : 10.10.10.6

root@m1: ~# ifconfig eth0:lan1 194.85.40.1 netmask 255.255.252.0
...

les machines configurées sur les mêmes sous réseaux sont capable de se joindre.

Supression de c2: ping pause, timeout
Voir screenshot de destination host unreachable
Remise du croisé: ping repart oklm

3.

tcpdump, route sur m1: voir screen

subnet 194.85.40.0 netmask 255.255.252.0 {
	range 194.85.40.1 194.85.43.200;
}
ddns-update-style none;

ajout de machines
m7 a été assigné une adresse sur lan1, m8 sur lan1, m9 sur lan2.

# TP 5.2
#### Création des VLAN
Séparation de 195.12.56.0/21 en deux sous-réseaux
NETMASK = 255.255.252.0
[195.12.56.0; 195.12.59.255] & [195.12.60.0; 195.12.63.255]

ifconfig eth0 195.12.56.1 netmask 255.255.252.0
...



__Creation des vlan__
vlan/create 1
vlan/create 2

__Assignation des vlan__
port/setvlan 1 1 => Associe le port 0001 au vlan 0001
Séparation des ports pour chaque VLAN, on constate bien que les deux sont maintenant bien isolés
