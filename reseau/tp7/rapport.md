# 1 - Message binaire
set xrange [-0.5:17] noreverse
set yrange [-0.5:2]

carre(x) = (0 < x && x < 1) || (2 < x && x < 4) || (6 < x && x < 7) || (8 < x && x < 11) || (12 < x && x < 13) || (14 < x && x < 16) ? 1 : 0
plot carre(x)

# 2 - Sinusoide

p(x) = sin(2*pi*x)
Division par 2 pour avoir amplitude de 1
+0.5 pour aligner

# Modulation

M(x) = sin((2 * pi * x) + (1 - carre(x))*pi)

# Démodulation

demod_M(x) = M(x) * p(x) > 0 ? 1 : 0

# Bruit

lolrip

# Amélioration

bruit(x) = M(x) + b(x)
fmoyen(x) = sum[a=0:16] (x >= a && x < (a+1) ? moyenne(a, a+(pi/4)):0)
plot M(x), bruit(x), fmoyen(x) > 0 ? 0.5 : -0.5, carre(x)
moyenne(a, b) = 1.0 * (sum [t=0:N] bruit(a + 1.0*(b-a)*t/N))/(N+1)

