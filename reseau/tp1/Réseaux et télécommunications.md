# Réseaux et télécommunications

## Travaux Pratiques - Rapport

THIERNO SAÏDOU SOUMAH - FLORIAN EULA

---------------

##### Plan de cablage du réseau local

![](./plan.png)

##### Construction du réseau

Les quatres machines sont reliées à un switch ethernet, les mettant ainsi sur le même réseau. La plage attribuée à ce réseau sont les addresses allant de `192.168.2.0` à `192.168.2.254`, 255 étant l'adresse de broadcasting. L'autre réseau à été attribué les adresses `192.168.1.x`.
L'adresse séléctionnée pour notre machine est `192.168.2.3`, avec comme masque de réseau `255.255.255.0`, qu'on peut abbrevier `192.168.2.3/24`.

Afin de configurer la machine pour l'empêcher de reconfigurer automatiquement le réseau (via DHCP, par exemple), le service network-manager est stoppé. L'adresse locale est ensuite manuellement attribuée.

```bash
user@192.168.2.3:~# su
root@192.168.2.3:~# service NetworkManager stop
-- ou bien, alternativement
root@192.168.2.3:~# /etc/init.d/network-manager stop
root@192.168.2.3:~# ifconfig eth0 192.168.2.3/24
```

En éxecutant un `ping` sur une autre machine du réseau, on peut constater que le temps du premier ping est plus long que les pings suivants (~1.0 ms contre ~0.55ms pour les suivants). Cela est du au protocole ARP, cette machine n'ayant pas encore été découverte sur le réseau, ce premier ping prend un peu plus longtemps.

Puisque les deux sous réseaux sont séparés, ils nous faut quelque chose pour les relier. Nous avons donc une machine passerelle, ayant activé l'`IPv4 forwarding`. 

```bash
passerelle:~# echo 1 > /proc/sys/net/ipv4/ip_forward
```

Ensuite, on configure les interfaces physiques pour être la passerelle de son sous-réseau. Par convention, l'adresse de cette passerelle est l'adresse se terminant par 254 pour le sous réseau. Ici, `eth2` est physiquement relié au switch du réseau 1, on le configure donc pour prendre l'adresse `192.168.1.254/24`, et de même pour `eth1`

```bash
passerelle:~# ifconfig eth2 192.168.1.254/24
passerelle:~# ifconfig eth1 192.168.2.254/24
```

On peut vérifier que les interfaces sont bien configurées et capable de recevoir des données grâce à `mii-tool`

![](mii-tool.jpg)

On peut également lancer `wireshark` pour constater que les autres machines sur le réseau émettent des requêtes vers les autres machines du réseau. Principalement des requêtes ARP en broadcast, cherchant les autres machines sur le réseau, des pings ICMP, des requêtes DNS, etc.

Finalement, il ne reste plus qu'a indiquer l'adresse de la passerelle, et quand l'utiliser. Ainsi, on indique que pour toute adresse sur `192.168.1.0/24`, la passerelle est joignable sur `192.168.1.254`. (Puisque nous ne sommes pas connectés directement avec l'autre réseau, on a besoin de cette passerelle).

```bash
root@192.168.2.3:~#route add -net 192.168.1.0 netmask 255.255.255.0 gw 192.168.1.254
```

Les machines sur l'autre réseau sont maintenant joignables, et on peut constater cela avec `ping` ou bien `wireshark`.

![](wireshark.jpg)

##### Protocole ARP et fragmentation

En débranchant le cable réseau lors d'un ping, on peut constater que plus rien ne s'affiche dans le terminal. L'affiche continue une fois le cable rebranché, l'ID du ping ayant toujours été incrémenté pendant l'instant sans connectivité. On peut supposer que `ping` n'affiche rien si l'interface utilisée n'est pas disponible. Les timeouts sont toutesfois indiqués dans les statistiques finales.

##### Fichier hosts 

```
192.168.1.1 m1
192.168.1.2 m2
192.168.1.4 m4
```

On constate bien que les pings sur les machines appropriées marchent.

##### Tables ARP

Contenu de la table ARP

```
Address            HWType   HWaddress         Flags Mask
m1                 ether    00:23:ae:a8:5e:da C
m2                 ether    00:23:ae:a6:7c:8a C
m4                 ether    00:23:ae:a6:80:2f C
```

Contenu de la table ARP avec IP

```
Address            HWType   HWaddress         Flags Mask
192.168.1.1        ether    00:23:ae:a8:5e:da C
192.168.1.2        ether    00:23:ae:a6:7c:8a C
192.168.1.4        ether    00:23:ae:a6:80:2f C
```

Les machines que l'on à ping sont bien présentes, leurs addresses MAC sont les bonnes.

Pour supprimer les entrées de la table ARP, on doit les supprimer une par une, via `arp -d  <nom>`. On constate aussi que débrancher le cable ethernet vide la table ARP après quelques secondes.

##### Capture de trames ARP

Le champ `type` dans la trame ethernet pour la requète ARP a pour valeur 0x0800, ce qui correspond à un type IP. En revanche, dans la requete ICMP ECHO, `type` est maintenant 8, qui correspont à ECHO, et 0 dans la réponse, qui correspond à ECHO-REPLY.

Pour les requêtes ARP, on observe deux niveaux d'encapsulation, premièrement Ethernet puis directement ARP. Pour les requetes ICMP, on a trois niveaux d'encapsulation, Ethernet puis IPv4 puis ICMP.

##### Fragmentation

Si on cause une fragmentation, on voit que l'on reçoit premièrement une trampe IPv4, notée comme utilisant le protocole FragmentedIP, puis le reste de la requete ICMP.
Dans le deuxième cas, Si le MTU de l'émetteur est supérieur au MTU du récepteur et qu'il y a fragmentation, alors on observe une perte totale des packets, aucun n'étant recu. Il faut donc judicieusement configurer le MTU. La taille minimale est imposée par la spécification Ethernet.
Les informations pour reconstituer le message ECHO ICMP se trouvent à la fois dans les paquets IPv4 et ICMP reçus. Les paquets IPv4 ont leur flags mis à 0x01, ce qui indique que plus de fragments vont arriver. Le champ fragment offset nous permet de savoir quelle partie nous recevons, et le champ total length nous permettra de savoir quand le dernier paquet sera reçu. Le dernier paquet, ICMP, aura pour flag 0x00, indiquant la fin du message.