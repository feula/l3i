# Réseaux et télécommunications - TP3

## Rapport

##### Configurtion du réseau étendu
1. Activation du routage sur m3: `m3: ~# echo 1 > /proc/sys/net/ipv4/ip_forwarding`

Assignation des ip des machines
```
m1: ~# ifconfig eth0 192.168.1.1/24 broadcast 192.168.1.255
m2: ~# ifconfig eth0 192.168.1.2/24 broadcast 192.168.1.255
m3: ~# ifconfig eth0 192.168.1.254/24 broadcast 192.168.1.255
m3: ~# ifconfig eth1 10.10.255.254/16 broadcast 10.10.255.255
m4: ~# ifconfig eth0 10.10.0.4/16 broadcast 10.10.255.255
```

2. Modification des tables de routage
```
m1: ~# route add -net 10.10.0.0/16 gw 192.168.1.254
m2: ~# route add -net 10.10.0.0/16 gw 192.168.1.254
m4: ~# route add -net 192.168.1.0/24 gw 10.10.255.254
```
On n'a pas besoin de changer m3, elle est déjà sur les deux réseaux.

3. Les pings fonctionnent, on voit bien passer les requetes ICMP, dans les deux sens.

4. ```
   m1: ~# route del -net 10.10.0.0/16 gw 192.168.1.254
   m2: ~# route del -net 10.10.0.0/16 gw 192.168.1.254
   m4: ~# route del -net 192.168.1.0/24 gw 10.10.255.254
   ```

5. Cela ne change rien à notre réseau. Pour observer une différence, il faudrait par exemple 	avoir d'autres machines sur des réseaux différents, mais accessibles depuis la gateway par défaut que l'on à. Par exemple, assigner toutes les IPs d'internet à passer par notre gateway par défaut, qui elle même gérerait le routage. En supposant qu'on ait accès à internet.

##### Role d'ICMP
-  **m1 configurée sur eth0**: ping m1 -> m4 : network unreachable. m1 ne sait pas comment accéder à m4.
-  **m1 configurée pour utiliser m3 comme passerelle**: ping m1 -> m4 : on voit des requetes ARP émises par m1, cherchant la gateway. Aucune machine ne répond.
-  **m3 configurée sur eth0/eth1**: On voit des messages de broadcast, et des requetes ICMP passer. Pas de réponses.
-  **m3 active le routage**: m4 recoit enfin des requetes ARP, mais n'est pas configurée. On a donc m3 en train de chercher 10.10.0.4.
-  **m4 configurée sur eth0**: Les pings continuent, mais toujours pas de réponse car m4 ne sait pas comment atteindre m1
-  **m4 configurée pour utiliser m3 comme passerelle**: les pings fonctionnent, on voit maintenant passer les request et les reply
