package net.eula.tp1;

class AntenneAvecIntervalle {
    /**
     * Fréquence minimale, en MHz.
     */
    final static int FREQ_MIN = 300;

    /**
     * Fréquence maximale, en MHz
     */
    final static int FREQ_MAX = 3000;

    private Position _position;
    private int      _id;
    private Intervalle _plage;

    /**
     * Instancie une Antenne
     * @param id ID de cette Antenne
     * @param position Position de cette Antenne
     * @param basse Limite basse de la plage de fréquences émises, supérieure ou égale à Antenne.FREQ_MIN
     * @param haute Limite haute de la plage de fréquences émises, inférieure ou égale à Antenne.FREQ_MAX
     */
    public AntenneAvecIntervalle(int id, Position position, Intervalle plage) {        
        assert(plage.getBasse() >= Antenne.FREQ_MIN);
        assert(plage.getHaute() <= Antenne.FREQ_MAX);
        
        _position = position;
        _id = id;
        _plage = plage;
    }

    @Override public String toString() {
        return String.format("%d:%s@%s", _id, _plage, _position);
    }

    /**
     * Retourne la distance entre deux Antennes
     */
    public static double distance(AntenneAvecIntervalle lhs, AntenneAvecIntervalle rhs) {
        double dx = rhs.getPosition().getX() - lhs.getPosition().getX();
        double dy = rhs.getPosition().getY() - lhs.getPosition().getY();

        return Math.sqrt((dx * dx) + (dy * dy));
    }

    /**
     * Retourne la distance entre cette Antenne et l'Antenne <other>
     */
    public double distance(AntenneAvecIntervalle other) {
        return AntenneAvecIntervalle.distance(this, other);
    }

    /**
     * Retourne la largeur de la plage de fréquences de cette Antenne
     */
    public int largeurPlageFrequences() {
        return _plage.difference();
    }

    public Position getPosition() {
        return _position;
    }
    public int getId() {
        return _id;
    }
}