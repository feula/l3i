package net.eula.tp1;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ex1();
        ex1_intervalle();
    }

    private static void ex1() {
        System.out.println("======= Ex 1 =========");
        SortedSet<Antenne> data = new TreeSet<Antenne>(new Comparator<Antenne>() {
            @Override public int compare(Antenne lhs, Antenne rhs) {
                return lhs.getId() - rhs.getId();
            }
        });

        Antenne a1 = new Antenne(10, new Position(2, 4), 400, 2000);
        Antenne a2 = new Antenne(2, new Position(3.5, 8.5), 450, 2500);

        data.add(a1);
        data.add(a2);
        data.add(new Antenne(5, new Position(3, 4), 300, 2000));
        data.add(new Antenne(12, new Position(12, 12), 800, 801));

        System.out.println("\nAntenne#toString");
        for (Antenne item : data) {
            System.out.println(item);
        }

        System.out.println("\nAntenne#distance");
        System.out.println("Expected: 4.74...");
        System.out.println("Static: " + Antenne.distance(a1, a2));
        System.out.println("Member: " + a1.distance(a2));

        System.out.println("\nAntenne#largeurPlageFrequences");
        System.out.println("Expected: 1600");
        System.out.println(a1.largeurPlageFrequences());
    }

    private static void ex1_intervalle() {
        System.out.println("======= Ex 1 - Intervalle =========");
        SortedSet<AntenneAvecIntervalle> data = new TreeSet<AntenneAvecIntervalle>(new Comparator<AntenneAvecIntervalle>() {
            @Override public int compare(AntenneAvecIntervalle lhs, AntenneAvecIntervalle rhs) {
                return lhs.getId() - rhs.getId();
            }
        });

        AntenneAvecIntervalle a1 = new AntenneAvecIntervalle(10, new Position(2, 4), new Intervalle(400, 2000));
        AntenneAvecIntervalle a2 = new AntenneAvecIntervalle(2, new Position(3.5, 8.5), new Intervalle(450, 2500));

        data.add(a1);
        data.add(a2);
        data.add(new AntenneAvecIntervalle(5, new Position(3, 4), new Intervalle(300, 2000)));
        data.add(new AntenneAvecIntervalle(12, new Position(12, 12), new Intervalle(800, 801)));

        System.out.println("\nAntenne#toString");
        for (AntenneAvecIntervalle item : data) {
            System.out.println(item);
        }

        System.out.println("\nAntenne#distance");
        System.out.println("Expected: 4.74...");
        System.out.println("Static: " + AntenneAvecIntervalle.distance(a1, a2));
        System.out.println("Member: " + a1.distance(a2));

        System.out.println("\nAntenne#largeurPlageFrequences");
        System.out.println("Expected: 1600");
        System.out.println(a1.largeurPlageFrequences());
    }
}