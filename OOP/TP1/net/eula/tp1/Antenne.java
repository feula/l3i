package net.eula.tp1;

class Antenne {
    /**
     * Fréquence minimale, en MHz.
     */
    final static int FREQ_MIN = 300;

    /**
     * Fréquence maximale, en MHz
     */
    final static int FREQ_MAX = 3000;

    private Position _position;
    private int      _id;
    private int      _plage_haute;
    private int      _plage_basse;

    /**
     * Instancie une Antenne
     * @param id ID de cette Antenne
     * @param position Position de cette Antenne
     * @param basse Limite basse de la plage de fréquences émises, supérieure ou égale à Antenne.FREQ_MIN
     * @param haute Limite haute de la plage de fréquences émises, inférieure ou égale à Antenne.FREQ_MAX
     */
    public Antenne(int id, Position position, int basse, int haute) {
        assert(basse >= Antenne.FREQ_MIN);
        assert(haute <= Antenne.FREQ_MAX);
        assert(haute > basse);
        
        _position = position;
        _id = id;
        _plage_haute = haute;
        _plage_basse = basse;
    }

    @Override public String toString() {
        return String.format("%d:[%d-%d]@%s", _id, _plage_basse, _plage_haute, _position);
    }

    /**
     * Retourne la distance entre deux Antennes
     */
    public static double distance(Antenne lhs, Antenne rhs) {
        double dx = rhs.getPosition().getX() - lhs.getPosition().getX();
        double dy = rhs.getPosition().getY() - lhs.getPosition().getY();

        return Math.sqrt((dx * dx) + (dy * dy));
    }

    /**
     * Retourne la distance entre cette Antenne et l'Antenne <other>
     */
    public double distance(Antenne other) {
        return Antenne.distance(this, other);
    }

    /**
     * Retourne la largeur de la plage de fréquences de cette Antenne
     */
    public int largeurPlageFrequences() {
        return _plage_haute - _plage_basse;
    }

    public Position getPosition() {
        return _position;
    }

    public int getId() {
        return _id;
    }
}