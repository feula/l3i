package net.eula.tp1;

class Voiture implements Vehicule {
    protected Roue[] roues;
    protected Moteur moteur;

    public Voiture(Roue[] roues, Moteur moteur) {
        this.roues = roues;
        this.moteur = moteur;
    }

    public String composition() {
        return "car do";
    }
}