package net.eula.tp1;

class Intervalle {
    private int _basse;
    private int _haute;

    public Intervalle(int basse, int haute) {
        assert(haute > basse);

        _basse = basse;
        _haute = haute;
    }

    public int getBasse() {
        return _basse;
    }

    public int getHaute() {
        return _haute;
    }

    public int difference() {
        return _haute - _basse;
    }

    @Override public String toString() {
        return String.format("[%d - %d]", _basse, _haute);
    }
}