package net.eula.tp1;

/**
 * Représente une Position sur une carte 2-dimensionnelle
 */
class Position {
    /**
     * Coordonnées X
     */
    private double _x;
    /**
     * Coordonnées Y
     */
    private double _y;

    /**
     * Construit une Position aux coordonnées (<x>;<y>)
     */
    public Position(double x, double y) {
        _x = x;
        _y = y;
    }

    public double getX() {
        return _x;
    }

    public double getY() {
        return _y;
    }

    @Override public String toString() {
        return String.format("(%s; %s)", _x, _y);
    }
}