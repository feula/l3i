# TP3 - Pattern State

```java
interface Etat {
  public void demarrerMoteur();
  public void couperMoteur();
  public void validerAdresse();
  public void arriver();
  public void passerModeSportive();
  public void passerModeConduite();
}
```

```java
class EtatArret implements Etat {
  private GoogleCar _car;
  public EtatArret(GoogleCar car) {
    _car = car;
  }
  public void demarrerMoteur() { _car.changeEtatDemarré(); }
  public void couperMoteur()   {}
  public void validerAdresse() {}
  public void arriver()        {}
  public void passerModeSportive() {}
  public void passerModeConduite() { _car.setMode(Mode.Conduite); }
}

class EtatDemarré implements Etat {
  private GoogleCar _car;
  public EtatDémarré(GoogleCar car) {
    _car = car;
  }
  public void demarrerMoteur() {}
  public void couperMoteur() { _car.changeEtatArret(); }
  public void validerAdresse() { _car.changeEtatRoule(); }
  public void arriver() {}
  public void passerModeSportive() { }
  public void passerModeConduite() { _car.setMode(Mode.Conduite); }
}

class EtatRoule implements Etat {
  private GoogleCar _car;
  public EtatRoule(GoogleCar car) {
    _car = car;
  }
  
  public void demarrerMoteur() {}
  public void couperMoteur() { _car.changeEtatArret(); }
  public void validerAdresse() {}
  public void arriver() { _car.changeEtatDémarré(); }
  public void passerModeSportive() { _car.setMode(Mode.Sportif); }
  public void passerModeConduite() { _car.setMode(Mode.Conduite); }
}
```

```java
enum Mode {
  Conduite, Sportif
}

class GoogleCar {
  private Etat _roule;
  private Etat _arret;
  private Etat _démarré;
  private Etat _etat;
  private Mode _mode;
  
  public GoogleCar() {
    _roule = new EtatRoule(this);
    _arret = new EtatArret(this);
    _démarré = new EtatDémarré(this);
    
    _etat = _arret;
    _mode = Mode.Conduite;
  }
  
  public void setMode(Mode mode) {
    _mode = mode;
  }
  
  public void changeEtatDémarré() { _etat = _démarré; }
  public void changeEtatArret() { _etat = _arret; }
  public void changeEtatRoule() { _etat = _roule; }
  
  public void demarrerMoteur() {
    _etat.demarrerMoteur();
  }
  public void couperMoteur() {
    _etat.couperMoteur();
  }
  public void validerAdresse() {
    _etat.validerAdresse();
  }
  public void arriver() {
    _etat.arriver();
  }
}
```















