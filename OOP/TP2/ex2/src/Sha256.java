import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Sha256 implements Hasher {
    public byte[] getHashedBytes(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes());

            return md.digest(); 
        }
        catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }

        return new byte[0];
    }
}