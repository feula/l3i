class Security {
    private Hasher _hasher;
    private String _data;

    public Security(String data, Hasher algorithm) {
        _hasher = algorithm;
        _data = data;
    }

    public byte[] chiffrer() {
        if (_hasher == null)
            assert(true);

        return _hasher.getHashedBytes(_data);
    }

    public byte[] chiffrer(String data, Hasher algorithm) {
        if (algorithm == null)
            assert(true);

        return algorithm.getHashedBytes(data);
    }

    public void setHashAlgorithm(Hasher algorithm) {
        _hasher = algorithm;
    }
}