class Main {
    public static void main(String[] args) {
        Security sec = new Security("Chiffrer n'est pas hasher.", new MD5());
        System.out.println(String.format("MD5: %s", sec.chiffrer()));
        sec.setHashAlgorithm(new Sha256());
        System.out.println(String.format("SHA256: %s", sec.chiffrer()));
        //sec.setHashAlgorithm(new BCrypt());
        //System.out.println(String.format("BCrypt: %s", sec.chiffrer()));
    }
}

