import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

class UKDateFormatter implements IDateFormatter {
    public String getFormattedDate(Date date) {
        return DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK).format(date);
    }
}

