import java.util.Date;

interface IDateFormatter {
    public String getFormattedDate(Date date);
}