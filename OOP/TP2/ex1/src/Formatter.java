import java.util.Date;

class Formatter {
    private IDateFormatter _formatter;

    public void setFormatter(IDateFormatter formatter) {
        _formatter = formatter;
    }

    public String format(Date date) {
        if (_formatter == null)
            assert(true);
        
        return _formatter.getFormattedDate(date);
    }
}