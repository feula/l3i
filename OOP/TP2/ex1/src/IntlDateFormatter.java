import java.text.SimpleDateFormat;
import java.util.Date;

class IntlDateFormatter implements IDateFormatter {
    public String getFormattedDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
}