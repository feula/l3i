import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

class USDateFormatter implements IDateFormatter {
    public String getFormattedDate(Date date) {
        return DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(date);
    }
}