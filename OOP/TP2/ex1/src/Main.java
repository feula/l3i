import java.util.Date;

public class Main {
    public static void main(String [] args) {
        Formatter formatter = new Formatter();
        formatter.setFormatter(new UKDateFormatter());

        Date date = new Date(111, 8, 21);

        System.out.println("Date Format: UK");
        System.out.println(formatter.format(date));

        formatter.setFormatter(new IntlDateFormatter());
        System.out.println("Date Format: International");
        System.out.println(formatter.format(date));

        formatter.setFormatter(new USDateFormatter());
        System.out.println("Date Format: US");
        System.out.println(formatter.format(date));

    }
}