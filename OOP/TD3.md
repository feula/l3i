# TD3 - Pattern State
## Exercice 1 - Distributeur de bonbons

1.  ​
```java
abstract class Etat {
	private Distributeur _distri;
	private String _message;

	public void tourner_poignee();
	public void bonbon_restant();
	public void bonbon_vide();
	public void inserer_piece();
	public void annuler_piece();

	public void message() {
		System.out.println(_message);
	}
}
```

2.  ​
```java
class EtatAttente extends Etat{
	_message = "En Attente.";
	public void tourner_poignee() { /* NOOP */ }
	public void bonbon_restant() { /* NOOP */ }
	public void inserer_piece() {
		_distri.set_etat_possede_piece();
	}
	public void bonbon_vide() {}
	public void annuler_piece() {}
}

class EtatPossedePiece extends Etat{
	_message = "Possède une pièce.";
	public void tourner_poignee() { 
		_distri.set_etat_vendu();
	}
	public void bonbon_restant() { /* NOOP */ }
	public void inserer_piece() {}
	public void bonbon_vide() {}
	public void annuler_piece() {
		_distri.set_etat_attente();
	}
}

class EtatVendu extends Etat{
	_message = "Bonbon vendu.";
	public void tourner_poignee() { /* NOOP */ }
	public void bonbon_restant() { 
		_distri.set_etat_attente();
	}
	public void inserer_piece() {}
	public void bonbon_vide() {
		_distri.set_etat_vide();
	}
	public void annuler_piece() {}
}

class EtatVide extends Etat{
	_message = "Machine vide.";
	public void tourner_poignee() { /* NOOP */ }
	public void bonbon_restant() { /* NOOP */ }
	public void inserer_piece() {}
	public void bonbon_vide() {}
	public void annuler_piece() {}
}
```

3.  ​
```java
class Distributeur {
	private EtatVendu _etatVendu;
	private EtatVide _etatVide;
  private EtatPossedePiece _etatPossedePiece;
	private EtatAttente _etatAttente;
	private Etat _etatActif;
	private int _bonbons_restants;

	public Distributeur(int bonbons) {
		_bonbons_restants = bonbons;
		_etatVide = new EtatVide();
		_etatAttente = new EtatAttente();
		_etatPossedePiece = new EtatPossedePiece();
		_etatVendu = new EtatVendu();
		_etatActif = _etatAttente;
	}

	public String message() {
		_etatActif.message();
	}

	public void set_etat_attente() {
		_etatActif = _etatAttente;
	}

}
```