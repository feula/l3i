# TD4 - Observer

```java
interface Observer {
  public void update(Billet post);
}
```

```java
class ObserverRSS implements Observer {
  private String _flux;
  public ObserverRSS(String flux) {
    _flux = flux;
  } 
  
  public void update(Billet post) {
  	RSSUtils.push(_flux, post.getTitle(), post.getContent())  ;
  }
}
```

```java
class ObserverEmail implements Observer {
  private String _email;
  public ObserverEmail(String email) {
    _email = email;
  }
  
  public void update(Billet post) {
    EmailUtils.send(_email, post.getTitle(), post.getContent());
  }
}
```



```java
interface Sujet {
  public void enregistrerObservateur(Observer o);
  public void   supprimerObservateur(Observer o);
  public void    notifierObservateur();
}
```

```java
class Blog implements Sujet {
  private ArrayList<Observer> _observers;
  
  
  public Blog() {
    _observers = new ArrayList<Observer>();
  }
  
  public void enregistrerObservateur(Observer o) {
    _observers.add(o);
  }
  
  public void supprimerObservateur(Observer o) {
    _observers.remove(o);
  }
  
  public void notifierObservateur() {
    for (Observer it in _observers) {
      it.update();
    }
  }
}
```

