# TD n°2 : Pattern Strategy

## Exercice 1 (Game of Thrones)  
#### 1.
**ComportementPoignard**: Classe  
**Chevalier**:            Classe  
**ComportementArc**:      Classe  
**Personnage**:           Classe Abstraite (Chevalier/Roi/Reine l'étendent, contient une propriété)  
**MarcheurBlanc**:        Classe  
**Roi**:                  Classe  
**Reine**:                Classe  
**ComportementEpee**:     Classe  
**ComportementHache**:    Classe  
**ComportementArme**:     Interface (Base de tous les Comportements)  

#### 2.  
**Spécialisation**: (Chevalier, Roi, Reine, MarcheurBlanc) -> Personnage  
**Association**: (ComportementArme) -> Personnage  
**Réalisation**: (ComportementPoignard, ComportementArc, ComportementEpee, ComportementHache) -> ComportementArme 

#### 3.  
setArme est placée dans Personnage.

## Exercice 2 (Intelligence Artificielle)
#### 1.
```java
public interface Strategie {
    public void action();
}
```

#### 2.
```java
public class IA {
    private String _nom;
    private Strategie _strategie;

    public IA(String nom) {
        _nom = nom;
    }

    public void action() {
        System.out.println(_nom);
        if (_strategie != null) 
            _strategie.action();
    }

    public String getNom() {
        return _nom;
    }

    public void setNom(String nom) {
        _nom = nom;
    }

    public Strategie getStrategie() {
        return _strategie;
    }

    public void setStrategie(Strategie strat) {
        _strategie = strat;
    }
}
```

#### 3.
```java
public class StrategieAgressive implements Strategie {
    @Override public void action() {
        System.out.println("Agressif");
    }
} 

public class StrategieNormale implements Strategie {
    @Override public void action() {
        System.out.println("Normal");
    }
}

public class StrategieDefensive implements Strategie {
    @Override public void action() {
        Systme.out.println("Défensif");
    }
}
```

#### 4.
Output:

```
Chucky
Agressif
George
Défensif
AIUR
Normal
Nouvelles stratégies : 'Chucky' va fuir.
'George' va passer en mode berserk.
Chucky
Defensif
George
Agressif
AIUR
Normal
```

## Exercice 3 (Statistiques et algorithmes de tri)

```java
public interface Sorter<T implements Comparable> {
    public T[] sort(T[] input);
}

public class BubbleSorter<T implements Comparable> {
    public T[] sort(T[] input) {
        for (int i = input.length - 1; i > 0; --i) {
            for (int j = 0; j < i; ++j) {
                if (input[j] > input[j+1]) {
                    int temp = input[j];
                    input[j] = input[j+1];
                    input[j+1] = temp;
                }
            }
        }

        return input;
    }
}

public class Stat {
    private int[] tableau;

    public Stat(int taille) {
        tableau = new int[taille];
    }

    public int getMinimum() {}
    public int getMaximum() {}
    public double getMedianne(Sorter sortAlgorithm) {
        tableau = sortAlgorithm.sort(tableau);

        // Calcul de la médiane.
    }
}

exemple:

stat.getMedianne(new BubbleSorter<int>());
stat.getMedianne(new Sorter<int>() {
    public int[] sort(int[] input) {
        ...
    }
});
```

## Exercice 4 (Transport)
#### 1.
```java
class CheminUniversite {
    private Transport _transport;
    public void aller() {
        ...
    }
}

interface Transport {
    public void seDeplacer();
}

class Pedestre implements Transport {
    @Override public void seDeplacer() {
        utiliser_ses_petites_pattes();
    }
}

class Velo implements Transport {
    @Override public void seDeplacer() {
        appuyer_avec_ses_petits_mollets();
    }
}

class Train implements Transport {
    @Override public void seDeplacer() {
        frauder_la_sncf();
    }
}

class Clio implements Transport {
    @Override public void seDeplacer() {
        bah_non_elle_a_ete_volee_et_incendiee();
        new Pedestre().seDeplacer();
    }
}

class AudiTT implements Transport {
    @Override public void seDeplacer() {
        tu_crois_que_tu_te_gares_a_nantes_avec_une_tt_lol();
    }
}
```

#### 2.

```java
public static void main(String[] args) {
    CheminUniversite[] etudiants = new CheminUniversite[5] {
        new CheminUniversite(new Pedestre()),
        new CheminUniversite(new Velo()),
        new CheminUniversite(new Train()),
        new CheminUniversite(new Clio()),
        new CheminUniversite(new AudiTT())
    };
    // On suppose que seDeplacer() fait un kilometre, je sais pas. détail d'implémentation.
    // Le premier etudiant part à pied, prend le train, et arrive a pied.
    etudiant[0].aller();
    etudiant[0].setTransport(new Train());
    etudiant[0].aller();
    etudiant[0].setTransport(new Pedestre());
    etudiant[0].aller();
    // == etudiant[0].aller()

    // Le deuxieme étudiant va directement a l'université en vélo. puis rentre dedans à pied.
    etudiant[1].aller();
    etudiant[1].setTransport(new Pedestre());
    etudiant[1].aller();

    // Le troisieme prend le train, se fait virer car il n'a pas de billet.
    etudiant[2].aller();
    etudiant[2].setTransport(new Pedestre());
    etudiant[2].aller();
    etudiant[2].aller();
    etudiant[2].aller();
    etudiant[2].aller();
    // Bah oui, fallait pas se faire lacher au milieu de la campagne. Perso je rentrerais chez moi.

    // Le quatrième s'est fait voler sa Clio. Il est dégouté et reste chez lui.
    // Toute ressemblance avec des évenements rééls est parfaitement fortuite.
    etudiant[3].setTransport(new Pedestre());

    // Le cinquieme plante son Audi, vole la Clio du quatrième, et termine dans le mur du commissariat.
    etudiant[4].aller();
    etudiant[4].setTransport(new Pedestre());
    etudiant[4].aller();
    etudiant[4].setTransport(new Clio());
    etudiant[4].aller();
    etudiant[4].setTransport(new Pedestre());

    // Finalement, personne ne veut prendre le train, parce que c'est trop cher.
}
```

## Exercice 5 (Chiffrement)(Hachage en fait.)

```java
interface Hasher {
    public byte[] getHashedBytes(String input);  
}

class MD5 implements Hasher {
    public byte[] getHashedBytes(String input) {
        return "Utiliser MD5 en 2016, vraiment?".getBytes();
    }
}

class Sha256 implements Hasher {
    public byte[] getHashedBytes(String input) {
        ...
    }
}

class ...

class Security {
    private Hasher _hasher;
    private String _data;

    public Security(String data, Hasher algorithm) {
        _hasher = algorithm;
        _data = data;
    }

    public byte[] chiffrer() {
        if (_hasher == null)
            throw new DansQuelleDimensionTuFaisCaException();

        return _hasher.getHashedBytes(_data);
    }

    public byte[] chiffrer(String data, Hasher algorithm) {
        if (algorithm == null)
            throw new TuFaisExpresException();

        return algorithm.getHashedBytes(data);
    }

    public void setHashAlgorithm(Hasher algorithm) {
        _hasher = algorithm;
    }
}

class Main {
    public static void main(String[] args) {
        Security sec = new Security("Chiffrer n'est pas hasher.", new MD5());
        System.out.println(String.format("MD5: %s", sec.chiffrer()))
        System.out.println(String.format("SHA256: %s", sec.chiffrer("Chiffrer n'est pas hasher.", new Sha256())));
        sec.setHashAlgorithm(new BCrypt());
        System.out.println(String.format("BCrypt: %s", sec.chiffrer()));
    }
}
```