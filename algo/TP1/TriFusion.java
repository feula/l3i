import java.util.*;


class TriFusion {
	public static void main(String[] args) {
		List<Integer> data = Arrays.asList(
			10, 25, 48, 21,
			43, 66, 12, 8, 
			-4, 9,  26, 92, 
			111, -5, -1, 22 
		);

		print_list(data, "Liste non triée");
		print_list(tri_fusion(data), "Liste triée");
	}

	private static void print_list(List<Integer> liste, String message) {
		System.out.println(message);
		for (int item : liste) {
			System.out.print(item + ", ");
		}
		System.out.println();
	}

	private static List<Integer> tri_fusion(List<Integer> liste) {
		// Liste déjà triée si il y a un seul élément
		List<Integer> output = new ArrayList<Integer>();
		if (liste.size() <= 1)
			return liste;
		
		List<Integer> left = liste.subList(0, liste.size() / 2);
		List<Integer> right = liste.subList(liste.size() / 2, liste.size());

		left = tri_fusion(left);
		right = tri_fusion(right);

		output.addAll(merge(left, right));

		return output;
	}

	private static List<Integer> merge(List<Integer> left, List<Integer> right) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		int idx_left = 0;
		int idx_right = 0;

		while (idx_left < left.size() && idx_right < right.size()) {	
			if (left.get(idx_left) <= right.get(idx_right)) {
				result.add(left.get(idx_left++));
			}		
			else {
				result.add(right.get(idx_right++));
			}
		}

		while (idx_left < left.size()) {
			result.add(left.get(idx_left++));
		}
		while (idx_right < right.size()) {
			result.add(right.get(idx_right++));
		}
		
		return result;		
	}
}