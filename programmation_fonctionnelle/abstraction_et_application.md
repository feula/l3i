# Abstraction et application

## Fonction

Voici trois expressions:

```ocaml
2 + (3 * 4) ;;

5 + (0 * 8) ;;

4 + (1 * 7) ;;
```

À noter : les doubles points-virgules ne font pas partie des expressions. Ils ne sont là que pour permettre leur évaluation par un simple *copier-coller* vers l'interprète.

Voilà trois *expressions* ayant les mêmes *valeurs*, formées à l'aide de *définitions locales* :

```ocaml
let
  a , b , c = 2 , 3 , 4
in
  a + (b * c) ;;

let
  a , b , c = 5 , 0 , 8
in
  a + (b * c) ;;

let
  a , b , c = 4 , 1 , 7
in
  a + (b * c) ;;
```

Étape d'**abstraction** :

```ocaml
function a , b , c -> a + (b * c) ;;
```

La *valeur* de cette *expression* est une **valeur fonctionnelle**, usuellement appelée **fonction**. En effet, son *type* est

```ocaml
int * int * int -> int
```

Or **une valeur est fonctionnelle quand son type comporte au moins une flèche**.

Étape d'**application** (trois fois) :

```ocaml
(function a , b , c -> a + (b * c)) (2 , 3 , 4) ;;

(function a , b , c -> a + (b * c)) (5 , 0 , 8) ;;

(function a , b , c -> a + (b * c)) (4 , 1 , 7) ;;
```

Nous retrouvons nos trois *valeurs*.Nous pourrions en *construire* bien d'autres.

En effet, nous disposons maintenant d'un **moyen de construire** des *valeurs* partageant ce que nous avons jugé commun aux trois *expressions* initiales.

Ce moyen est la *valeur fonctionnelle*

```ocaml
function a , b , c -> a + (b * c)
```

**Ce qui fait d'elle un moyen de construction effectif, c'est le fait que**, à la différence des autres *valeurs*, **les valeurs fonctionnelles peuvent être appliquées**.

**Appliquer une fonction, c'est construire une valeur à partir d'une autre valeur** (qui est ici un triplet de *valeurs* de *type*  `int` ).



___



## Fonction d'ordre supérieur

Voici encore trois *expressions* :

```ocaml
cos (0.7854) *. cos (1.0472) ;;

sin (1.0472) *. cos (3.1416) ;;

sin (0.5236) *. sin (0.7854) ;;
```

Voilà trois *expressions* ayant les mêmes *valeurs*, formées à l'aide de *définitions locales* :

```ocaml
let
  a , b , c , d = cos , 0.7854 , cos , 1.0472
in
  a (b) *. c (d) ;;

let
  a , b , c , d = sin , 1.0472 , cos , 3.1416
in
  a (b) *. c (d) ;;

let
  a , b , c , d = sin , 0.5236 , sin , 0.7854
in
  a (b) *. c (d) ;;
```

Étape d'**abstraction** :

```ocaml
function a , b , c , d -> a (b) *. c (d) ;;
```

La *valeur* de cette *expression* est une **valeur fonctionnelle (ou fonction) d'ordre supérieur**. En effet, son *type* est

```ocaml
('a -> float) * 'a * ('b -> float) * 'b -> float
```

Or **une valeur est fonctionnelle d'ordre supérieur quand son type comporte plus d'une flèche**.

Étape d'**application** (trois fois) :

```ocaml
(function a , b , c , d -> a (b) *. c (d)) (cos , 0.7854 , cos , 1.0472) ;;

(function a , b , c , d -> a (b) *. c (d)) (sin , 1.0472 , cos , 3.1416) ;;

(function a , b , c , d -> a (b) *. c (d)) (sin , 0.5236 , sin , 0.7854) ;;
```

Nous retrouvons nos trois *valeurs*.

Et surtout, nous disposons d'un **moyen de construire** des *valeurs* (partageant ce que nous avons jugé commun aux trois *expressions* initiales) :

```ocaml
function a , b , c , d -> a (b) *. c (d)
```

*Appliquer* cette *fonction*, c'est *construire* une *valeur* à partir d'une autre *valeur* (qui est ici un quadruplet dont deux des composantes sont, elles aussi, des *fonctions*). 



___



## Fonction d'ordre supérieur encore

Voici encore trois *expressions* :

```ocaml
function z -> 2 * z ;;

function z -> 3 * z ;;

function z -> 4 * z ;;
```

Voilà trois *expressions* ayant les mêmes *valeurs*, formées à l'aide de *définitions locales* :

```ocaml
let
  a = 2
in
  function z -> a * z 
;;
```

```ocaml
let
  a = 3
in
  function z -> a * z 
;;
```

```ocaml
let
  a = 4
in
  function z -> a * z 
;;
```



Étape d'**abstraction** :

```ocaml
function a -> (function z -> a * z) ;;
```

La *valeur* de cette *expression* est une **fonction d'ordre supérieur**. En effet, son *type* comporte plus d'une flèche :

```ocaml
int -> (int -> int)
```

Étape d'**application** (trois fois) :

```ocaml
(function a -> (function z -> a * z)) (2) ;;

(function a -> (function z -> a * z)) (3) ;;

(function a -> (function z -> a * z)) (4) ;;
```

Nous retrouvons nos trois *valeurs*.

Et surtout, nous disposons d'un **moyen de construire** des *valeurs* partageant ce que nous avons jugé commun aux trois *expressions* initiales.

Comme cet aspect commun s'écrit

```ocaml
function z -> a * z
```

le moyen dont nous disposons :

```ocaml
function a -> (function z -> a * z)
```

est un **moyen de construire des fonctions**. Il permet par exemple de construire

```ocaml
(function a -> (function z -> a * z)) (-1) ;;
```

qui permet de calculer (*construire*) l'opposée d'une *valeur* de type  `int` , par exemple l'opposée de  `5`  :

```ocaml
((function a -> (function z -> a * z)) (-1)) (5) ;;
```

 

___



## Fonction polymorphe d'ordre supérieur

À plusieurs reprises, nous allons *lier un nom à une valeur* (de façon non-locale). Nous pourrions nous passer de telles *liaisons*, comme précédemment. Mais elles permettront d'alléger l'écriture.

Commençons par ces deux *fonctions d'ordre supérieur* (nous avons déjà rencontré la première) :

```ocaml
let produit_par = function a -> (function z -> a * z) ;;

let ajout_de = function a -> (function z -> a + z) ;;
```

Considérons maintenant les deux *expressions* dont les *valeurs* sont *liées* aux *noms*  `produit_n_aire`  et  `somme_n_aire`  :

```ocaml

let produit_n_aire =
	let rec f = function
        lx -> match lx with
        	[] -> 1
            | x :: lx' -> (produit_par (x)) (f (lx'))
in
  f ;;

let somme_n_aire =
	let rec f = function
        lx -> match lx with
        	[] -> 0
            | x :: lx' -> (ajout_de (x)) (f (lx'))
in
  f ;;
```

La *valeur* de la première (resp. seconde) *expression* est une *fonction* permettant de calculer (*construire*), étant donné une *liste* de *type*  `int list` , le produit (resp. la somme) des *valeurs* de la liste si celle-ci est non vide, la valeur  `1`  (resp. `0` ) sinon :

```ocaml
produit_n_aire ([1 ; 2 ; 3 ; 4 ; 5]) ;;

somme_n_aire ([1 ; 2 ; 3 ; 4 ; 5]) ;;
```

Transformons chacune de ces deux *expressions* en une *expression* ayant la même *valeur*, formée à l'aide de *définitions locales* imbriquées :

```ocaml
let a = produit_par
in
  let
    b = 1
  in
    let rec f = function
    	lx -> match lx with
        	[] -> b
            | x :: lx' -> (a (x)) (f (lx'))
    in
      f ;;

let a = ajout_de
in
  let
    b = 0
  in
    let rec f = function
    	lx -> match lx with
        	[] -> b
            | x :: lx' -> (a (x)) (f (lx'))
    in
      f ;;
```

Double étape d'**abstraction** :

```ocaml
let accumule = function
  a -> function
  b -> let rec
  f = function
  	lx -> match lx with
    	[] -> b
        | x :: lx' -> (a (x)) (f (lx'))
in
	f ;;
```

La *valeur* de l'*expression* *liée* au *nom*  `accumule`  est une **fonction d'ordre supérieur**. En effet, son *type* comporte plus d'une flèche :

```ocaml
('a -> ('b -> 'b)) -> ('b -> ('a list -> 'b))
```

De plus, elle est **polymorphe**, puisqu'**une valeur est polymorphe quand son type comporte au moins une variable de type**. Ici il y en a deux, nommées  `a`  et  `b`  (deux degrés de liberté).

Étape d'**application** (deux fois) :

```ocaml
(accumule (produit_par)) (1) ;;

(accumule (ajout_de)) (0) ;;
```

Nous retrouvons les *valeurs liées aux noms*  `produit_n_aire`  et  `somme_n_aire` . Appliquons-les :

```ocaml
((accumule (produit_par)) (1)) ([1 ; 2 ; 3 ; 4 ; 5]) ;;

((accumule (ajout_de)) (0)) ([1 ; 2 ; 3 ; 4 ; 5]) ;;
```

Et surtout, nous disposons d'un **moyen de construire** des *valeurs* (partageant ce que nous avons jugé commun aux *expressions* initiales des *valeurs liées aux noms*  `produit_n_aire`  et  `somme_n_aire` ).

Il s'agit d'un **moyen polymorphe de construire des fonctions à partir de fonctions** :

```ocaml
let concatenation_n_aire =
	(accumule (function x -> (function y -> x ^ y))) ("") 
;;

concatenation_n_aire (["O" ; "bje" ; "ctiv" ; "e " ; "C" ; "am" ; "l"]) ;;

let conjonction_n_aire , disjonction_n_aire =
	(accumule (function x -> (function y -> x && y))) (true) ,
	(accumule (function x -> (function y -> x || y))) (false) 
;;

let
  lb = [true = not (false) ; 1 + 1 = 3 ; "oui" = "ou" ^ "i"]
in
  conjonction_n_aire (lb) , disjonction_n_aire (lb) ;;

let rond = function g -> (function f -> (function x -> g (f (x)))) ;;

let composition_n_aire =
	function lf -> ((accumule (rond)) (function x -> x)) (lf) 
;;

let 
  h = composition_n_aire ([produit_par (3) ; produit_par (2) ; ajout_de (1)])
in
  (h (2) , h (7)) ;;
```

Remarque : l'écriture

```ocaml
let conjonction_n_aire , disjonction_n_aire =
	(accumule (rond)) (function x -> x)
;;
```

La *fonction*  `accumule`  constitue donc un moyen très puissant de *construction* de *fonctions* très utiles[.](undefined)

Il n'est donc pas étonnant que le module  `List`  d'*Objective Caml* offre des *fonctions polymorphes* d'accumulation, dont  `fold_left`  et  `fold_right`  :

```ocaml
let
  z1 = ((accumule (produit_par)) (1)) ([1 ; 2 ; 3 ; 4 ; 5])
  and
  z2 = ((List.fold_left (produit_par)) (1)) ([1 ; 2 ; 3 ; 4 ; 5])
  and
  z3 = ((List.fold_right (produit_par)) ([1 ; 2 ; 3 ; 4 ; 5])) (1)
in
  z1 , z2 , z3 ;;
```

Attention à la non-commutativité :

```ocaml

let
  f = function x -> (function y -> x - y)
in
  let
    z1 = ((accumule (f)) (10)) ([1 ; 2 ; 3 ; 4 ; 5])
    and
    z2 = ((List.fold_left (f)) (10)) ([1 ; 2 ; 3 ; 4 ; 5])
    and
    z3 = ((List.fold_right (f)) ([1 ; 2 ; 3 ; 4 ; 5])) (10)
  in
    z1 , z2 , z3 
;;
```

```ocaml
let
  f = function x -> (function y -> y - x)
in
  let
    z1 = ((accumule (f)) (10)) ([1 ; 2 ; 3 ; 4 ; 5])
    and
    z2 = ((List.fold_left (f)) (10)) ([1 ; 2 ; 3 ; 4 ; 5])
    and
    z3 = ((List.fold_right (f)) ([1 ; 2 ; 3 ; 4 ; 5])) (10)
  in
    z1 , z2 , z3 
;;
```

```ocaml
let
  f = function x -> (function y -> x ^ y)
  and
  lch = ["O" ; "bje" ; "ctiv" ; "e "]
in
  let
    ch1 = ((accumule (f)) ("Caml")) (lch)
    and
    ch2 = ((List.fold_left (f)) ("Caml")) (lch)
    and
    ch3 = ((List.fold_right (f)) (lch)) ("Caml")
  in
    ch1 , ch2 , ch3 
;;
```

```ocaml
let
  f = function x -> (function y -> y ^ x)
  and
  lch = ["O" ; "bje" ; "ctiv" ; "e "]
in
  let
    ch1 = ((accumule (f)) ("Caml")) (lch)
    and
    ch2 = ((List.fold_left (f)) ("Caml")) (lch)
    and
    ch3 = ((List.fold_right (f)) (lch)) ("Caml")
  in
    ch1 , ch2 , ch3 
;;
```





___



## Fonction polymorphe d'ordre supérieur encore

Ici aussi, à plusieurs reprises, nous allons *lier un nom à une valeur* (de façon non-locale), pour alléger l'écriture.

D'abord :

```ocaml
let plus_R =
	function x , y -> x +. y ;;

let fois_R =
	function x , y -> x *. y ;;
```

Considérons maintenant les deux *expressions* dont les *valeurs* sont *liées* aux *noms* `somme_de_fonctions_de_R_dans_R`  et  `produit_de_fonctions_de_R_dans_R`  :

```ocaml

let somme_de_fonctions_de_R_dans_R =
	function f , g -> (function x -> plus_R (f (x) , g (x))) 
;;

let produit_de_fonctions_de_R_dans_R =
	function f , g -> (function x -> fois_R (f (x) , g (x))) 
;;
```

En fait, leur *type* est

```ocaml
('a -> float) * ('a -> float) -> ('a -> float)
```

et non

```ocaml
(float -> float) * (float -> float) -> (float -> float)
```

Appliquons-les :

```ocaml
let
  f = function x -> 2. *. x
  and
  g = function x -> x +. 1.
in
  let
    h = somme_de_fonctions_de_R_dans_R (f , g)
    and
    k = produit_de_fonctions_de_R_dans_R (f , g)
  in
    h (5.) , k (-3.) 
;;
```

Voilà deux *expressions* ayant les mêmes *valeurs*, formées à l'aide de *définitions locales* :

```ocaml
let
  op = plus_R
in
  function f , g -> (function x -> op (f (x) , g (x))) 
;;

let
  op = fois_R
in
  function f , g -> (function x -> op (f (x) , g (x)))
;;
```

Étape d'**abstraction** :

```ocaml
let phi
=
function op -> (function f , g -> (function x -> op (f (x) , g (x)))) ;;
```

La *valeur* de cette *expression* est une **fonction polymorphe d'ordre supérieur**. En effet, son *type* comporte plus d'une flèche et au moins une *variable de type* :

​		`('a * 'b -> 'c) -> (('d -> 'a) * ('d -> 'b) -> ('d -> 'c))`

Étape d'**application** (deux fois) :

```ocaml
let somme_de_fonctions_de_R_dans_R' =
	function t -> (phi (plus_R)) (t) 
;;

let produit_de_fonctions_de_R_dans_R' =
	function t -> (phi (fois_R)) (t) 
;;
```

Nous retrouvons les *valeurs liées* aux *noms*  `somme_de_fonctions_de_R_dans_R`  et  `produit_de_fonctions_de_R_dans_R` . Appliquons-les :

```ocaml
let
  f = function x -> 2. *. x
  and
  g = function x -> x +. 1.
in
  let
    h = somme_de_fonctions_de_R_dans_R' (f , g)
    and
    k = produit_de_fonctions_de_R_dans_R' (f , g)
  in
    h (5.) , k (-3.) 
;;
```

Et surtout, nous disposons d'un **moyen de construire** des *valeurs* (partageant ce que nous avons jugé commun aux *expressions* initiales des *valeurs liées aux noms* `somme_de_fonctions_de_R_dans_R`  et  `produit_de_fonctions_de_R_dans_R` ).

Il s'agit d'un **moyen polymorphe de construire des fonctions à partir de fonctions** :

```ocaml
let sup_de_fonctions_de_R_dans_R =
	function t -> (phi (function x , y -> (max (x)) (y))) (t) 
;;

let
  f = function x -> 2. *. x
  and
  g = function x -> x +. 1.
in
  (sup_de_fonctions_de_R_dans_R (f , g)) (2.) 
;;
```

En fait, le *type* de  `sup_de_fonctions_de_R_dans_R`  est

​					`('a -> 'b) * ('a -> 'b) -> ('a -> 'b)`

et non

​			`(float -> float) * (float -> float) -> (float -> float)`



___



## Bilan

Dans chacun des exemples précédents, il y a d'abord une étape d'*abstraction* à partir des deux ou trois *expressions* initiales.

L'**abstraction** consiste à :

	1. remarquer ue les *structures* des *expressions* sont similaires ;
	2. séparer ce qui leur est commun de ce ui leur est spécifiue ;
	3. Transformer
     	1. ce qqui leur est spécifique en une expression `e` (usuellement appelée *paramètre*) ;
     	2. ce qui lleur est commun en une expression`e'` ;
	4. produire l'*expression* suivante, à *valeur fonctionnelle* : `function e -> e'`.

La démarche est la même que la *fonction* obtenue soit *d'ordre supérieur* ou non.

Dans chacun des exemples précédents, il y a ensuite une étape d'*application* de la *fonction* obtenue à ce qui est spécifique à chacune des deux ou trois *expressions* initiales (et qu'on appelle usuellement *argument* de la *fonction*). Elle permet de retrouver chacune de leurs *valeurs*.

L'**application** de `function e -> e'` consiste à :

	1. remplacer le *paramètre* `e` par la *valeur* `v` de l'*argument* (on dit usuellement *lier* le paramètre à la valeur de l'argument ) ;
	2. *évaluer* `e` dans ce *contexte* (ou *environnement*).

Ce qui revient à *évaluer* `let e = v in e'`.