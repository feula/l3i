# Type Liste

## Constructeurs

```ocaml
[] : unit -> list			(*Constructeur de base, la liste vide*)
:: : 'a * 'a list -> 'a list
```

## Sélecteurs

```ocaml
hd : 'a list -> 'a			(*Premier élément de la liste*)
tl : 'a list -> 'a list		(*Liste moins de premier élément*)
```

## Prédicat

```ocaml
length : 'a list -> int		(*Taille de la liste*)
```

Lors de l'utilisation des listes utiliser `open Liste;;` 

```ocaml
let rec longueur l =
	if l = [] then
		0
	else
		1+longueur(tl(l))
;;

(*Version très longue de Toto*)
let rec longueur l =
	if l = [] then
		0
	else
		let 
			x = hd(l) and l'= tl(l)
		in 
			let
				long = longueur(l')
			in
				1 + long
;;
```

## Fonctions

### est_membre

```ocaml
est_membre : 'a liste -> bool

let rec est_membre (a,l) =
	if l = [] then
		false
	else if hd(l) = a then
		true
	else
		est_membre(a,tl(l))
;;
```

### jusqua

```ocaml
jusqua : int -> int list (* 0 -> []
							3 -> [3,2,1]*)

let rec jusqua (x) = 
	if x = 0 then
		[]
	else
		x::jusqua(x-1)
;;
```

