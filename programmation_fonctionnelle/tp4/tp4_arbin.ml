(* Fonctions à définir 
nb_noeuds_arbin: 'a arbin -> int
nb_feuilles_arbin: 'a arbin -> int
est_membre_arbin: 'a * 'a arbin -> bool
Hauteur de l'arbre en niveaux de noeuds
hauteur_arbin: 'a arbin -> int

Renvoie un arbre ou la valeur de tous les noeuds est doublée

      3                       6
    /   \                   /   \
   5     7         ->      10    7
    \   / \                 \   / \
     4 6   2                 8 12  4

arbin_des_doubles: int arbin -> int arbin

Renvoie un arbre ou la valeur des noeuds sont la hauteur des arbres dans les noeuds

     V                       0
   /   \                   /   \
  E     E         ->      4     1       Avec E un arbin quelconque
   \   / \                 \   / \
    E E   E                 5 7   2

arbin_des_hauteurs: 'a arbin arbin -> int arbin

 Fusionne deux arbres en renvoyant un arbre dont les noeuds sont un couple de valeurs 
 Les arbres doivent être similaires 
rassemble_arbin: 'a arbin * 'b arbin -> ('a*'b) arbin

 Sépare un arbre relié par rassemble_arbin en les deux arbres originaux 
 réciproque de rassemble_arbin 
separe_arbin: ('a*'b) arbin -> 'a arbin * 'b arbin

 Pour aller plus loin, faire une fonction map_arbin, pour par exemple faire arbin_des_doubles et arbin_des_hauteurs ezpz 
 spec 
arbin_vide: Unit -> 'a arbin
embranche_bin: 'a * 'a arbin * 'a arbin -> 'a arbin
est_arbin_vide: 'a arbin -> bool
rac: 'a arbin -> 'a
sag: 'a arbin -> 'a arbin
sad: 'a arbin -> 'a arbin
 ****************************************
 **************************************** *)

let ab1 = embranche_bin (1, embranche_bin (2, arbin_vide (), arbin_vide ()), embranche_bin (5, arbin_vide (), arbin_vide ())) ;;
let ab2 = embranche_bin (7, embranche_bin (0, arbin_vide (), arbin_vide ()), embranche_bin (8, arbin_vide (), arbin_vide ())) ;;
let ab3 = embranche_bin (4, embranche_bin (11, arbin_vide (), arbin_vide ()), arbin_vide ()) ;;
let ab4 = embranche_bin (6, embranche_bin (10, arbin_vide (), ab3), embranche_bin (9, ab2, embranche_bin (3, ab3, ab1))) ;;

(* **************************************** *)
(* **************************************** *)
let arb = embranche_bin(ab1, embranche_bin(ab2, arbin_vide(), arbin_vide()), embranche_bin(ab3, embranche_bin(ab4, arbin_vide(), arbin_vide()), arbin_vide()));;


let rec nb_noeuds_arbin arbre =
	if (est_arbin_vide(arbre)) then
		0
	else
		1 + nb_noeuds_arbin(sag(arbre)) + nb_noeuds_arbin(sad(arbre))
;;
nb_noeuds_arbin(ab4);;

let rec nb_feuilles_arbin arbre =
	if est_arbin_vide(arbre) then
		0
	else if not(est_arbin_vide(sag(arbre))) || not(est_arbin_vide(sad(arbre))) then
		nb_feuilles_arbin(sag(arbre)) + nb_feuilles_arbin(sad(arbre))
	else
		1
;;	
nb_noeuds_arbin(ab4);;

let rec est_membre_arbin (value, arbre) =
	if est_arbin_vide(arbre) then
		false
	else if rac(arbre) = value then
		true
	else
		est_membre_arbin((value, sag(arbre))) || est_membre_arbin((value, sad(arbre)))
;;
est_membre_arbin(5, ab4);;

let rec hauteur_arbin arbre = 
	if est_arbin_vide(arbre) then
		0
	else
		max (1+hauteur_arbin(sag(arbre))) (1+hauteur_arbin(sad(arbre)))
;;
hauteur_arbin(ab4);;

let rec arbin_des_doubles arbre =
	if est_arbin_vide(arbre) then
		arbin_vide()
	else
		embranche_bin(2*rac(arbre), arbin_des_doubles(sag(arbre)), arbin_des_doubles(sad(arbre)))
;;
arbin_des_doubles(ab3);;

let rec arbin_des_hauteurs arbre = 
	if est_arbin_vide(arbre) then
		arbin_vide()
	else
		embranche_bin(hauteur_arbin(rac(arbre)), arbin_des_hauteurs(sag(arbre)), arbin_des_hauteurs(sad(arbre)))
;;
arbin_des_hauteurs(arb)
;;

let rec rassemble_arbin (a, b)= 
	if est_arbin_vide(a) || est_arbin_vide(b) then
		arbin_vide()
	else
		embranche_bin((rac(a), rac(b)), rassemble_arbin(sag(a), sag(b)), rassemble_arbin(sad(a), sad(b)))
;;
rassemble_arbin(ab1, ab2);;

let rec separe_bin a = 
	if est_arbin_vide(a) then
		(arbin_vide(), arbin_vide())
	else
		let (x, y) = rac(a) in
		let (gg, gd) = separe_bin(sag(a)) in
		let (dg, dd) = separe_bin(sad(a)) in
			(embranche_bin(x, gg, dg), embranche_bin(y, gd, dd))
;;
separe_bin(rassemble_arbin(ab1, ab2));;
