# Type Arbin

## Constructeurs

```ocaml
type 'a arbin = V | E of 'a * 'a arbin * 'a arbin
(*Constructeur de base, l'arbre binaire vide*)
arbin_vide : unit -> 'a arbin	
(*Créé un noeud de l'arbre binaire*)
embranche_bin : 'a * 'a arbin * 'a arbin -> E
```

## Sélecteurs

```ocaml
rac : 'a arbin -> 'a		(*Retourne l'élément de l'arbre binaire*)
sag : 'a arbin -> 'a arbin	(*Retourne le sous arbre gauche de l'arbre 								binaire courrant*)
sad : 'a arbin -> 'a arbin	(*Retourne le sous arbre droit de l'arbre 								binaire courrant*)
```

## Prédicat

Lors de l'utilisation des arbres binaires utiliser :

```ocaml
#load 'arbin.cmo';;
open arbin;;
```

