(* Créé un naturel à partir d'un int *)
let rec naturel_of_int x =
    if x > 0 then 
        Nat_1.suc(naturel_of_int(x-1))
    else
        Nat_1.zero()

(* Créé un entier à partir d'un Nat_1.naturel *)
let rec int_of_naturel x =
    if Nat_1.est_zero(x) then
        0
    else
        int_of_naturel(Nat_1.pre(x)) + 1

(* Renvoie true si x est pair, false sinon *)
let rec est_pair x = 
    if Nat_1.est_zero x then
        true
    else
        not(est_pair(Nat_1.pre(x)))

(* (x, y) -> x + y *)
let rec plus (x, y) =
    if not(Nat_1.est_zero y) && not(Nat_1.est_zero x) then
        Nat_1.suc(Nat_1.suc(plus(Nat_1.pre(x), Nat_1.pre(y))))
    else if Nat_1.est_zero y then
        x
    else if Nat_1.est_zero x then
        y
    else
        Nat_1.zero()

let rec fois (x, y) =
    if Nat_1.est_zero y || Nat_1.est_zero x then
        Nat_1.zero()
    else
        plus(fois(x, Nat_1.pre(y)), x)

let rec supegal (x, y) =
    if Nat_1.est_zero x && Nat_1.est_zero y then
        true
    else if Nat_1.est_zero y then
        true
    else if Nat_1.est_zero x then
        false
    else
        supegal(Nat_1.pre(x), Nat_1.pre(y))

let rec moins (x, y) =
    if supegal(y, x) then
        (* impossible de representer les négatifs *)
        Nat_1.zero()
    else if Nat_1.est_zero y then
        x
    else
        moins(Nat_1.pre(x), Nat_1.pre(y))

let rec quotient_et_reste (x, y) =
    (0,0)

let rec est_puissance_de_deux x = 
    false

let rec log_base_deux x =
    Nat_1.zero()
     
