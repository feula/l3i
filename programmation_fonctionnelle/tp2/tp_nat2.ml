let rec naturel_of_int x =
    if x = 1 then
        Nat_2.un()
    else if x = 0 then
        Nat_2.zero()
    else
        Nat_2.sucsuc(naturel_of_int(x-2))
        
let rec int_of_naturel x =
    if Nat_2.est_zero x then
        0
    else if Nat_2.est_un x then
        1
    else
        int_of_naturel(Nat_2.prepre(x)) + 2

let rec est_pair x = 
    if Nat_2.est_zero x then true
    else if Nat_2.est_un x then false
    else est_pair(Nat_2.prepre(x))

let rec plus (x, y) =
    (* Résultats évidents *)
    if Nat_2.est_zero x then 
        (* 0 + y *)
        y
    else if Nat_2.est_zero y then
        (* x + 0 *)
        x
    else if Nat_2.est_un x && Nat_2.est_zero y then
        (* 1 + 0 *)
        Nat_2.un()
    else if Nat_2.est_zero x && Nat_2.est_un y then
        (* 0 + 1 *)
        Nat_2.un()
    else if Nat_2.est_un x && Nat_2.est_un y then
        (* 1 + 1 *)
        Nat_2.sucsuc(Nat_2.zero())
    else
        if Nat_2.est_un(y) && not(Nat_2.est_un x) then
            Nat_2.sucsuc(plus(Nat_2.prepre(x), y))
        else if Nat_2.est_un(x) && not(Nat_2.est_un y) then
            Nat_2.sucsuc(plus(x, Nat_2.prepre(y)))
        else
            Nat_2.sucsuc(Nat_2.sucsuc(plus(Nat_2.prepre(x), Nat_2.prepre(y))))
    
let rec fois (x, y) = 
    if Nat_2.est_zero y || Nat_2.est_zero x then
        Nat_2.zero()
    else if Nat_2.est_un x then
        x
    else if Nat_2.est_un y then
        x
    else
        plus(fois(x, Nat_2.prepre(y)), plus(x,x))

let rec supegal(x, y) =
    if Nat_2.est_zero x && Nat_2.est_zero y then
        true
    else if Nat_2.est_un x && Nat_2.est_un y then
        true
    else if Nat_2.est_zero y then
        true
    else if Nat_2.est_zero x then
        false
    else if Nat_2.est_un x && not(Nat_2.est_un y) then
        false
    else
        supegal(Nat_2.prepre(x), Nat_2.prepre(y))

let rec moins(x, y) = 
    if supegal(y, x) then
        Nat_2.zero()
    else if Nat_2.est_zero y then
        x
    else if Nat_2.est_un y then
        x
    else 
        moins(Nat_2.prepre(x), Nat_2.prepre(y))