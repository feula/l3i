# TP2 - Utilisation de flex
## Premier programme

test.txt
```c
if (bob == true) {
	return "Bobatronic";
}
else if (bob == false){
	return "Bobbify";
}
else if (bob == file_not_found) {
	return "."
}
else {
    return 42
}
```

```
./ex1 < test.txt
```
Après:
```c
si (bob == true) {
        return "Bobatronic";
}
sinon si (bob == false){
        return "Bobbsiy";
}
sinon si (bob == file_not_found) {
        return "."
}
sinon {
    return 42
}
```

## 2.1
```
~$ ./ex2 < test.txt
Nombres : 1, Mots : 20
```

## 2.2

```yacc
%{
    char* yacc;
    char* from;
    char* plus;
    char* identifier;
    char* floating_point;
%}
Y \b(Yacc)\b
F ^(From)\b
P \b.*\+{3,10}.*\b
I \b[a-Z_][a-Z0-9_]*\b
V \b[0-9]+\.[0-9]+\b
%%
%%
```