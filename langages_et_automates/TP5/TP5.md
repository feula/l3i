# TP5 - Automates à Etats Finis
## Exercice 1
![](images/abimpair.png)
Expression régulière: `a*|a*b(bb)*`

## Exercice 2
**Rouge**: Plus rien à consommer, pas sur un état final. L'automate ne valide pas cette chaine.  
**Vert**: Plus rien à consommer, sur un état final. L'automate valide cette chaine.  
**Bleu**: Encore des caractères à consommer

**aa**  
```
q0 -> q1 (-> q11) -> q8
q0 -> q1 (-> q11) -> q12
```

**aabbb**
```
q0 -> q1 -> q8 -> q6 -> q7 -> q8
```

**a**: rejeté.  

**aaaaa**: rejeté.

Expression rationnelle: `((aa)+|(aaa)+)((bb)+|(bbb)+)`

## Exercice 3
**aaa**: Rejeté par les deux  
**ab**: Accepté par les deux, une possibilité  
**abbaabb**: Accepté par les deux, une possibilité  
**abaab**: Rejeté par les deux

Expression régulière: `a+b+(a+(bb)*)*`

## Exercice 4
**aa**: Accepté, une configuration  
**babbaa**: Accepté, une configuration  
**ab**: Rejeté  
**bab**: Rejeté

Expression rationelle: `(((a+b)ba*b)*(a+b)aa*ba*b)*((a+b)ba*b)*(a+b)aa*`
# WHAT

## Exercice 5

```
S -> aS
S -> aA
A -> bA
A -> bB
B -> aC
B -> 
C -> bA
C -> B
C -> 
```

Ceci est une grammaire de type 3. ma gueule.  
Elle l'est déjà bob arrètes la moquette.