

import sys

def get_word_count(input):
    return str(len(input.split(" ")))

if __name__ == "__main__":
    if (len(sys.argv) != 2):
        exit(-1)

    file = open(sys.argv[1])
    print(get_word_count(file.read()) + " " + sys.argv[1])
    file.close()