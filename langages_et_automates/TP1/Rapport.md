# TP1 - Utilisation des expressions régulieres
## 1.1 - Compter les mots

166 681 mots selon wc 
```
feula@hipsterbox$ wc -w germinal.html
166681 germinal.html
```

165 975 mots selon libreoffice  
![](./libreoffice.png)

181 517 mots selon GEdit  
![](./gedit.png)

**get_words.py**
```python
import sys

def get_word_count(input):
    return str(len(input.split(" ")))

if __name__ == "__main__":
    if (len(sys.argv) != 2):
        return 0

    file = open(sys.argv[1])
    print(get_word_count(file.read()) + " " + sys.argv[1])
    file.close()
```

162 910 mots selon mon script
```
feula@hipsterbox$ python ./get_words.py germinal.html
162910 germinal.html
```

## 1.2.1 - grep

1. Mots commençant par p ou P et terminant par e
```
grep "\b(p|P)\w+e\b" germinal.html -Eon
```

2. Nombres
```
grep "[0-9]*" germinal.html -Eon
```

3. Formes du verbe regarder
```
grep "\bregard(e|es|ons|ez|ent|ais|ait|ions|iez|aient|ai|as|a|âmes|âtes|èrent|erai|eras|era|erons|erez|é)\b" germinal.html -Eon
```

4. Mots commençant par une majuscule
```
grep "[A-Z]\w+" germinal.html -Eon
```

5. Mots contenant un double t
```
grep "\b\w+?(tt)\w+\b" germinal.html -Eon
```

6. Mots entre 10 et 20 lettres
```
grep "\b\w{10,20}\b" germinal.html -Eon
```

7. Occurrences du mot "est" et leurs contextes gauche et droit de 21 caractères.
```
grep ".{21}\best\b.{21}" germinal.html -Eon
```

On peut bien rechercher les nombres impairs car on sait que leur dernier caractère sera {1;3;5;7;9}.

```shell
grep "[0-9]*[1|3|5|7|9]" germinal.html -Eon
```

On ne peut pas vérifier que les parenthèses sont bien équilibrées, cela demanderait de la récursion, ou au moins un stack, ce qu'un regex ne nous fournit pas.  
On ne peut pas trouver les suites de nombres corissants, un regex ne nous permet pas de comparer avec les matchs précédents.

## 1.2.2 - LibreOffice

Pas grand chose à dire, mêmes résultats