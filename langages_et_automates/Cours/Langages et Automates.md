# Langages et Automates

## Ensembles

### Définitions

> Collection d'objets uniques d'un univers $U$. Chaque objet d'un ensemble est appelé **élément** de cet ensemble. Soit $E$ un ensembles, $a$ un élément de $E$, on note $a \in E$.

**Définition par extension**: $J = \{lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche\}$

**Définition par intension**: $J = \{j | j \ est \ un \ jour \ de \ la \ semaine\}$

**Ensemble vide**: L'ensemble vide ne contient aucun élément. Il est note $\{\}$ ou $\emptyset$.

---------

### Opérations sur les ensembles

Soient $A$, $B$ des ensembles dans un univers $U$.

##### Complément

Eléments de U non présents dans A
$\bar A$ ou $Comp(A) = \{x | x \in U \ et \ x \notin A\}$

##### Union

Eléments présents dans soit dans A ou B ou les deux
 $A \cup B = \{x | x \in A \ ou\ x \in B\}$

##### Intersection

Eléments présents à la fois dans A et dans B
 $A \cap B = \{ x | x \in A \ et\ x \in B\}$ 

##### Différence / Exclusion

Eléments présents dans A mais pas dans B
$A\ \backslash\ B = \{ x | x \in A \ et\ x \notin B\}$

-------------

### Propriétés des ensembles

Soient $A, B, C$ des ensembles de l'univers $U$

##### Associativité

$(A \cup B) \cup C = A \cup (B \cup C)$
$(A \cap B) \cap C = A \cap (B \cap C)$

##### Commutativité

$A \cup B = B \cup A$
$A \cap B = B \cap A$

##### Complémentation

$A \cup \bar A = U$
$A \cap \bar A = \emptyset$ 

##### Idempotence

$A \cup A = A$
$A \cap A = A$

##### Identité

$A \cup \emptyset = A$
$A \cap U = A$

##### Extremité

$A \cup U = U$
$A \cap \emptyset = \emptyset$

##### Involution

$\overline {(\overline A)} = A$

##### Lois de Morgan

$\overline {(A \cup B)} = \overline A \cap \overline B$
$\overline {(A \cap B)} = \overline A \cup \overline B$

##### Distributivité

$A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$
$A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$

----------

### Réccurrence simple

##### Principe

Soit un ensemble infini de propositions $\{T_n | n \in \mathbb N\} = \{T_0; T_1; T_2; ...; Tn\}$
$\forall n \in \mathbb N, T_n$ est vraie si et seulement si:

* $T_0$ est vraie.
* $\forall k \in \mathbb N$, si $T_k$ est vraie alors $T_{k+1}$ est vraie.

##### Démonstration par induction simple

La proposition doit être vraie pour $n = 0$.

1. **Démontrer que $T_0$ est vraie**

Supposer que la proposition est vraie pour $k \in \mathbb N$ et passer à $k+1$:

2. **Enoncer l'hypothèse d'induction**: $T_k$ est vraie.
3. **Démontrer que $T_{k+1}$ est vraie** en utilisant l'hypothèse d'induction
4. **Conclusion**: la proposition est vraie $\forall n \in \mathbb N$ par induction simple

##### Exemple

Soit la proprieté $T_n: n(n+1)/2 = \sum_{1\leq i\leq n}{i} = 1 + 2 +3 + ... + n$
Montrons par une récurrence simple que cette proprieté est vraie pour tous les entiers positifs ou nuls.

**Montrons que $T_0$ est vraie:** pour $T_0$ : $0(0+1)/2 = 0$ et $\sum_{1 \leq i \leq 0}{i} = \sum \emptyset = 0$
Donc $T_0$ est vraie.

**Hypothèse d'induction**: pour $k \in \mathbb N$, supposons que $T_k$ est vraie.

**Démonstration**: Montrons que $T_{k+1}$ est vraie en suivant l'hypothèse d'induction.
$$
\ \ (k+1)((k+1) + 1)/2 =  (k(k+2) +k +2)/2 \\
= k(k+1)/2 + (k + k + 2)/2 \\
= k(k+1)/2 +k + 1
$$
Or $T_k$ est vraie: $k(k+1) / 2 = \sum_{1 \leq i \leq k}{i}$
Donc $(k+1)((k+1)+1)/2 = \sum_{1 \leq i \leq k}{i} +k +1 = \sum_{1 \leq i \leq k+1}{i}$
Donc $T_{k+1}$ est vraie

**Conclusion**: $\forall n \in \mathbb N, T_n$ est vraie.

------------

### Réccurrence généralisée

##### Principe

Soit un ensemble de propositions $\{T_n | n \in \mathbb N\}$
$\forall n \in \mathbb N, T_n$ est vraie si et seulement si: 

1. $\forall k \in \mathbb N$, si $T_i$ sont vraies $\forall i \lt k$ alors $T_k$ est vraie.

##### Démonstration par induction généralisée

Supposer que la proposition est vraie $\forall i, 0 \leq i \leq k$ et passer à $k$. Enoncer l'hypothèse d'induction. Montrer que $T_k$ est vraie. Conclure.

Il n'y a pas de cas de base à traiter mais il est souvent utile de traiter $T_0$ à part.

##### Exemple

> Sur l'exemple $T_n : n(n+1)/2 = \sum_{1 \leq i \leq n}i = 1 + 2+ 3 + ... + n$
> Montrons par une récurrence généralisée que cette proprieté est vraie pour tous les entiers positifs ou nuls.
>
> 1. Soit $k \in \mathbb N$. Nous supposons que $\forall i, 0 \leq 1 \lt k, T_i$ est vraie. Montrons sous cette hypothèse que $T_k$ est vraie.
>    * Si $k = 0$ : dans ce cas, l'hypothèse d'induction est vide  mais $0(0+1)/2 = 0$ et $\sum_{1 \leq i \leq 0}i = \sum \emptyset = 0$
>      $\implies T_k$ est vraie.
>    * Si $k > 0$,
>      $T_k: k(k+1)/2 = k(k-1+2)/2 = (k(k-1)+2k)/2 = (k-1)k/2 + k$
>      Or $0 \leq k -1 \lt k$. Donc $T_{k-1}$ est vraie et $(k-1)k/2 = \sum_{1 \leq i \leq k-1}i$
>      $\implies k(k+1)/2 = \sum_{1 \leq i \leq k-1}{i+k} = \sum_{1 \leq i \leq k}i$
>      $\implies T_k$ est vraie (dans le cas ou $k \gt 0$)
>      $\implies T_k$ est vraie (dans tous les cas sous l'hypothèse d'induction)
> 2. **Conclusion**: $\forall n \in \mathbb N, T_n$ est vraie.

---------

## Généralités sur les langages

### Monoïdes

>  Un **monoïde** est un ensemble $E$ muni d'une opération binaire interne associative $\oplus$ et possédant un élément neutre $\epsilon$.
>  Notation: $<E, \oplus, \epsilon >$

##### Exemples

- $< \mathbb N, +, 0 >$ les entiers positifs ou nuls avec l'addition (élément neutre: 0)
- $< \mathbb Z, +, 0>$ les entiers relatifs avec l'addition (élément neutre: 0)
- $<\mathbb R, \times, 1 >$ les nombres rééls avec la multiplication (élément neutre: 1)
- $< P(U), \cup, \emptyset >$  les sous ensembles de l'univers $U$ avec l'union
- ...

##### Proprietés d'un monoïde

* $\oplus$ est une **opération binaire interne** (ou **stable**) à E:
  $$
  \forall x, y \in E, x \oplus y \ est\ défini\ et\ x \oplus y \in E
  $$

* $\oplus$ est **associative**:
  $$
  \forall x, y, z \in E, (x \oplus y) \oplus z = x \oplus (y \oplus z)
  $$
  Pour cette raison, les parenthèses sont souvent omises.

* $\epsilon$ est un élément neutre de $E$ pour $\oplus$
  $$
  \epsilon \in E\ et\ \forall x \in E, \epsilon \oplus x = x \oplus \epsilon = x
  $$

* Il ne peut exister qu'un seul élément neutre dans un monoïde. Si $\epsilon$ et $\epsilon'$ sont deux éléments neutres de $E$ pour $\oplus$ , alors $\epsilon = \epsilon \oplus \epsilon' = \epsilon'$

###  Sous-monoïde

> *Soit* $< E, \oplus, \epsilon >$ un monoïde et $T$ un sous-ensemble de $E (T \subseteq E)$.
> $<T, \oplus, \epsilon>$ est un sous-monoïde de $<E, \oplus, \epsilon>$ si et seulement si c'est un monoïde.
> Puisque $<E, \oplus, \epsilon>$ est un monoïde, il suffit de démontrer que:
>
> * $\epsilon \in T$
> * $\oplus$ est stable dans $T$: $\forall t, t' \in T, t \oplus t' \in T$

### Monoïde engendré

> Soit $M = <E, \oplus, \epsilon>$ un monoïde. Pour toute partie $A$ de $E$, on peut définir le plus petit sous-monoïde de $M$ contenant $A$. On l'appelle le **sous monoïde de $M$ engendré par $A$**.



## Langages Rationnels

### Langage rationnel

#### Définition

> Soit $A$ un **alphabet** . Les langages rationnels sur $A$ sont les éléments de la classe $Rat(A^*)$ définie récursivement de la façon suivante: $Rat(A^*)$ est le plus petit sous ensemble de $P(A)$ tel que
>
> * $\emptyset \in Rat(A^*)$ 
> * $\{\epsilon\} \in Rat(A^*)$ 
> * $\forall a \in A^*, \{a\} \in Rat(A^*)$
> * **Union**: Si $L_1 \in Rat(A^*)$ et $L_2 \in Rat(A^*)$ alors $L_1 \cup L_2 \in Rat(A^*)$
> * **Produit**: Si $L_1 \in Rat(A^*)$ et $L_2 \in Rat(A^*)$ alors $L_1 \cap L_2 \in Rat(A^*)$
> * **Fermeture**: Si $L_1 \in Rat(A^*)$ alors $L_1^* \in Rat(A^*)$

#### Théorème des parties finies

> Toute partie finie $L$ de $A^*$ est dans $Rat(A^*)$

​	**Démonstration**: un langage fini est l'union des langages contenant un mot du langage.

#### Exemples de langages rationnels

* L'alphabet $A = \{0; 1\}$, le langage décrivant les symboles de la table ASCII en binaire. Ce langage est fini.
* L'alphabet $A = \{0; 1\}$, le langage décrivant les chaines de caractères en binaire. Ce langage est fini dans certains langages (ex: Pascal, 256 caractères) ou délimité (C, terminé par 0x00).

### Expression rationnelle

#### Définition

> Les expressions rationelles sur $A$ décrivent les langages rationnels. Elles sont définites de la façon suivante.
>
> * $\emptyset$ est une expression rationnelle qui décrit le langage $\emptyset$
> * $\epsilon$ est une expression rationnelle qui décrit le langage $\epsilon$
> * $\forall a \in A$, $a$ est une expression rationnelle qui décrit le langage $\{a\}$
> * Si $l_1$ et $l_2$ sont des expressions rationnelles qui décrivent $L_1$ et $L_2 \in Rat(A^*)$ alors
>   * $(l_1|l_2)$ est une expression rationnelle qui décrit le langage $L_1 \cup L_2 \in Rat(A^*)$ 
>   * $(l_1 . l_2)$ est une expression rationnelle qui décrit le langage $L_1 \times L_2 \in Rat(A^*)$
>   * $l^*_1$ est une expression rationnelle qui décrit le langage rationnel $L_1^* \in Rat(A^*)$

#### Notations et proprietés

* **Préséance**: $ * \ >\ .\ >\ | $
* **Opérateur $^+$**: $a^+ = a.a^*$
* **Opérateur $?$**: $a^? = (a|\epsilon)$

#### Théorème des expression rationnelles

> Tout langage dénoté par une expression rationnelle est un langage rationnel et tout langage rationnel est dénoté par une expression rationnelle.

#### Expression rationnelle standard

> Une expression rationnelle est dite **standard** si et seulement si les seuls opérateurs utilisés sont $.$, $|$ et $^*$ 

#### Equivalence entre expression rationnelles

> Deux expression rationnelles $w_1$ et $w_2$ sont dites **équivalentes** si elles décrivent le même langage rationnel

#### Propriétés

