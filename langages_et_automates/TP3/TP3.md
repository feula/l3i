# TP3 - Utilisation des grammaires de type 0-3

## Exercice 1
### 2.
`L = a|b`
### 3.
Ce langage possède une grammaire de type 0 car la règle `bA -> b` est une règle ou la partie de gauche est plus grande que la partie de droite.
### 5.
`a`  
![](images/a.png)  
`b`  
![](images/b.png)  
L'utilisation d'une règle contextuelle est représentée par une barre bleue.

----------------
## Exercice 2
### 1.
C'est une grammaire de type 0.
### 2.
Oui, `aabbcc` fait bien partie du langage de cette grammaire.
### 3.
![](images/lol.png)  
Cet arbre à 51 noeuds

----------------
## Exercice 3
### 1.
```
S -> aSb|ab
```
### 2.
> This is a Context-Free Grammar

\#bam
### 3.
`aaabbb` en fait bien partie.
```
S -> aSb -> aaSbb -> aaabbb
```

### 4.
![](images/aaabbb.png)  
Il possède 4 noeuds.

### 5.
![](images/multiple.png)

### 6.
Les algorithmes proposés sont:
 * Descending Brute Force 
 * Cocke-Youger-Kasami
 * Tables LL
 * Tables SLR
 * User Control
----------------
## Exercice 4
### 1.
Ce langage est équivalent à `a*b+`.

### 2.
```
S -> aS|A
A -> bB|B
B -> b|bB
```
### 3.
JFLAP confirme que cette grammaire est bien de type 3
> This is a right-linear Grammar (Regular Grammar and Context-Free Grammar)

\#tupeuxpastest
### 4.
![](images/bbb.png)  
![](images/aabbb.png)  
![](images/aaa.png)



